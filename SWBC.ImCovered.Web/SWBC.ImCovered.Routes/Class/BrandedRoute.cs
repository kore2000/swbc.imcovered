﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Routing;
using System.Linq;

using SWBC.ImCovered.Routes.Interfaces;

namespace SWBC.ImCovered.Routes.Class
{
    public class BrandedRoute :
        IBrandedRoute
    {
        #region <Private members>

        private static readonly string _defaultBrand;
        private readonly IRouter _router;

        #endregion

        #region <Constructor>

        static BrandedRoute()
        {
            _defaultBrand = "default";
        }

        public BrandedRoute(
            IRouter pRouter)
        {
            _router = pRouter ??
                throw new ArgumentNullException(
                    nameof(pRouter));
        }

        #endregion

        #region <IRouter>

        public VirtualPathData GetVirtualPath(
            VirtualPathContext pContext)
        {
            return (_router.GetVirtualPath(pContext));
        }

        public async Task RouteAsync(
            RouteContext pContext)
        {
            var hostAddress = pContext.HttpContext.Request.Host;

            if (!hostAddress.HasValue)
            {
                await _router.RouteAsync(pContext);
                return;
            }

            string[] splitHost = hostAddress.Host.Split('.');
            splitHost = splitHost
                .Where(w => !w.Equals(
                    "www",
                    StringComparison.CurrentCultureIgnoreCase))
                .ToArray();
            if (splitHost.Length > 0 &&
                splitHost.Length <= 2)
            {
                await _router.RouteAsync(pContext);
                return;
            }

            if (splitHost.Length > 2)
            {
                var pulledBrand = splitHost.FirstOrDefault();
                if (string.IsNullOrWhiteSpace(pulledBrand))
                {
                    pulledBrand = _defaultBrand;
                }
                pContext
                    .RouteData
                    .Values
                    .Add("brand", pulledBrand);
            }

            await _router.RouteAsync(pContext);
        }

        #endregion
    }
}
