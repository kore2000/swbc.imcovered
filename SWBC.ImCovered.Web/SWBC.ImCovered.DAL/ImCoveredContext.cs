﻿using Microsoft.EntityFrameworkCore;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;
using SWBC.ImCovered.DAL.Classes.Mapping;

namespace SWBC.ImCovered.DAL
{
    public class ImCoveredContext :
        DbContext
    {
        #region <Public accessors>

        public DbSet<Account> Accounts
        {
            get; set;
        }
        public DbSet<Agency> Agencies
        {
            get; set;
        }
        public DbSet<Language> Languages
        {
            get; set;
        }
        public DbSet<Brand> Brands
        {
            get; set;
        }
        public DbSet<UrlShortName> UrlShortNames
        {
            get; set;
        }

        #endregion

        #region <Constructors>

        public ImCoveredContext()
        {
        }

        public ImCoveredContext(
            DbContextOptions<ImCoveredContext> pOptions) :
            base(pOptions)
        {
        }

        #endregion

        protected override void OnModelCreating(
            ModelBuilder pBuilder)
        {
            pBuilder.ApplyConfiguration(new AccountEntityConfig());
            pBuilder.ApplyConfiguration(new AgencyEntityConfig());
            pBuilder.ApplyConfiguration(new CoBrandEntityConfig());
            pBuilder.ApplyConfiguration(new LanguageEntityConfig());
            pBuilder.ApplyConfiguration(new UrlShortNameEntityConfig());
        }
    }
}
