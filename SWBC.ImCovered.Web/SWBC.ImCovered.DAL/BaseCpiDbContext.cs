﻿using Microsoft.EntityFrameworkCore;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;
using SWBC.ImCovered.DAL.Classes.Mapping;
using SWBC.ImCovered.Enums.Classes;

namespace SWBC.ImCovered.DAL
{
    public abstract class BaseCpiDbContext :
        DbContext
    {
        #region <Private members>

        private readonly DbContextEnums.AgencyNumber _agency;

        #endregion

        #region <Public accessors>

        public DbContextEnums.AgencyNumber Agency
        {
            get => (_agency);
        }

        public DbSet<CpiAccountInformationFI100> CpiFI100
        {
            get; set;
        }

        public DbSet<CpiAccountInformationFI104> CpiFI104
        {
            get; set;
        }

        #endregion

        #region <Constructors>

        protected BaseCpiDbContext()
        {
        }

        protected BaseCpiDbContext(
            DbContextEnums.AgencyNumber pAgency,
            DbContextOptions<Cpi99DbContext> pOptions) :
            base(pOptions)
        {
            _agency = pAgency;
        }

        protected BaseCpiDbContext(
            DbContextEnums.AgencyNumber pAgency,
            DbContextOptions<Cpi01DbContext> pOptions) :
            base(pOptions)
        {
            _agency = pAgency;
        }

        #endregion

        protected override void OnModelCreating(
            ModelBuilder pBuilder)
        {
            pBuilder.ApplyConfiguration(new CpiAccountInformationFI104EntityConfig());
            pBuilder.ApplyConfiguration(new CpiAccountInformationFI00EntityConfig());
        }
    }
}
