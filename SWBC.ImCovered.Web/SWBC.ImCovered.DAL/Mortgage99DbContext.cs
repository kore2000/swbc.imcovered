﻿using Microsoft.EntityFrameworkCore;

using SWBC.ImCovered.Enums.Classes;

namespace SWBC.ImCovered.DAL
{
    public class Mortgage99DbContext :
        BaseMortgageDbContext
    {
        #region <Constructors>

        public Mortgage99DbContext()
        {
        }

        public Mortgage99DbContext(
            DbContextOptions<Mortgage99DbContext> pOptions) :
            base(
                DbContextEnums.AgencyNumber.AgencyNinetyNine,
                pOptions)
        {
        }

        #endregion
    }
}
