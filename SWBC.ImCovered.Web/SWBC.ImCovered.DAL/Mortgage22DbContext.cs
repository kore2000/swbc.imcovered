﻿using Microsoft.EntityFrameworkCore;

using SWBC.ImCovered.Enums.Classes;

namespace SWBC.ImCovered.DAL
{
    public class Mortgage22DbContext :
        BaseMortgageDbContext
    {
        #region <Constructors>

        public Mortgage22DbContext()
        {
        }

        public Mortgage22DbContext(
            DbContextOptions<Mortgage22DbContext> pOptions) :
            base(
                DbContextEnums.AgencyNumber.AgencyTwentyTwo,
                pOptions)
        {
        }

        #endregion
    }
}
