﻿using Microsoft.EntityFrameworkCore;

using SWBC.ImCovered.Enums.Classes;

namespace SWBC.ImCovered.DAL
{
    public class Cpi99DbContext :
        BaseCpiDbContext
    {
        #region <Constructors>

        public Cpi99DbContext()
        {
        }

        public Cpi99DbContext(
            DbContextOptions<Cpi99DbContext> pOptions) :
            base(
                DbContextEnums.AgencyNumber.AgencyNinetyNine,
                pOptions)
        {
        }

        #endregion
    }
}
