﻿using Microsoft.EntityFrameworkCore;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;
using SWBC.ImCovered.DAL.Classes.Mapping;
using SWBC.ImCovered.Enums.Classes;
using SWBC.ImCovered.DAL.Classes.StoreProcResults;

namespace SWBC.ImCovered.DAL
{
    public abstract class BaseMortgageDbContext :
        DbContext
    {
        #region <Private members>

        private readonly DbContextEnums.AgencyNumber _agency;

        #endregion

        #region <Public accessors>

        public DbContextEnums.AgencyNumber Agency
        {
            get => (_agency);
        }

        public DbSet<MortgageAccountInformationFMI001> MortgageFMI001
        {
            get; set;
        }

        public DbSet<MortgageAccountInformationFMI104> MortgageFMI104
        {
            get; set;
        }

        public DbQuery<MortgageLookupSpResult> MortgageLookupSP
        {
            get; set;
        }

        #endregion

        #region <Constructors>

        protected BaseMortgageDbContext()
        {
        }

        protected BaseMortgageDbContext(
            DbContextEnums.AgencyNumber pAgency,
            DbContextOptions<Mortgage01DbContext> pOptions) :
            base(pOptions)
        {
            _agency = pAgency;
        }

        protected BaseMortgageDbContext(
            DbContextEnums.AgencyNumber pAgency,
            DbContextOptions<Mortgage21DbContext> pOptions) :
            base(pOptions)
        {
            _agency = pAgency;
        }

        protected BaseMortgageDbContext(
            DbContextEnums.AgencyNumber pAgency,
            DbContextOptions<Mortgage22DbContext> pOptions) :
            base(pOptions)
        {
            _agency = pAgency;
        }

        protected BaseMortgageDbContext(
            DbContextEnums.AgencyNumber pAgency,
            DbContextOptions<Mortgage99DbContext> pOptions) :
            base(pOptions)
        {
            _agency = pAgency;
        }

        #endregion

        protected override void OnModelCreating(
            ModelBuilder pBuilder)
        {
            pBuilder.ApplyConfiguration(new MortgageAccountInformationFMI001EntityConfig());
            pBuilder.ApplyConfiguration(new MortgageAccountInformationFMI104EntityConfig());
            pBuilder.ApplyConfiguration(new MortgageLookupSpResultQueryConfig());
        }
    }
}
