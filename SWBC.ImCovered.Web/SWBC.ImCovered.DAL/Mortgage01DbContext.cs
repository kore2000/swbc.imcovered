﻿using Microsoft.EntityFrameworkCore;

using SWBC.ImCovered.Enums.Classes;

namespace SWBC.ImCovered.DAL
{
    public class Mortgage01DbContext :
        BaseMortgageDbContext
    {
        #region <Constructors>

        public Mortgage01DbContext()
        {
        }

        public Mortgage01DbContext(
            DbContextOptions<Mortgage01DbContext> pOptions) :
            base(
                DbContextEnums.AgencyNumber.AgencyOne,
                pOptions)
        {
        }

        #endregion
    }
}
