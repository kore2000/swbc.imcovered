﻿using Microsoft.EntityFrameworkCore;

using SWBC.ImCovered.Enums.Classes;

namespace SWBC.ImCovered.DAL
{
    public class Cpi01DbContext :
        BaseCpiDbContext
    {
        #region <Constructors>

        public Cpi01DbContext()
        {
        }

        public Cpi01DbContext(
            DbContextOptions<Cpi01DbContext> pOptions) :
            base(
                DbContextEnums.AgencyNumber.AgencyOne,
                pOptions)
        {
        }

        #endregion
    }
}
