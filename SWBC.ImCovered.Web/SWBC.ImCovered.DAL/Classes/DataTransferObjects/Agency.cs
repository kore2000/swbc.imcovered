﻿using System;
using System.Collections.Generic;

namespace SWBC.ImCovered.DAL.Classes.DataTransferObjects
{
    [Serializable]
    public class Agency
    {
        #region <Private members>

        private int _id;
        private string _name;

        private ICollection<Account> _accounts;

        #endregion

        #region <Public accessors>

        public int Id
        {
            get => (_id);
            set => _id = value;
        }

        public string Name
        {
            get => (_name);
            set => _name = value;
        }

        public ICollection<Account> Accounts
        {
            get => (_accounts);
            set => _accounts = value;
        }

        #endregion
    }
}
