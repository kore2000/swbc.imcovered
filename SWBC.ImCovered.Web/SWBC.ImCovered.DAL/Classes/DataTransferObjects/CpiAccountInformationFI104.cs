﻿using System;

namespace SWBC.ImCovered.DAL.Classes.DataTransferObjects
{
    [Serializable]
    public class CpiAccountInformationFI104
    {
        #region <Private members>

        private double? _referenceNumber;
        private int _accountNumber;
        private int _loanSuffix;
        private string _loanNumber;

        #endregion

        #region <Public accessors>

        public string JoinedPrimaryKey
        {
            get => ($"{_accountNumber}||{_loanNumber}||{_loanSuffix}");
        }

        public double? ReferenceNumber
        {
            get => (_referenceNumber);
            set => _referenceNumber = value;
        }

        public int AccountNumber
        {
            get => (_accountNumber);
            set => _accountNumber = value;
        }

        public int LoanSuffix
        {
            get => (_loanSuffix);
            set => _loanSuffix = value;
        }

        public string LoanNumber
        {
            get => (_loanNumber);
            set => _loanNumber = value;
        }

        #endregion
    }
}
