﻿using System;

using SWBC.ImCovered.Enums.Classes;

namespace SWBC.ImCovered.DAL.Classes.DataTransferObjects
{
    [Serializable]
    public abstract class BaseAccountInformation
    {
        #region <Protected members>

        protected DbContextEnums.AgencyNumber _agency;
        protected int _accountNumber;
        protected int _loanSuffix;
        protected string _loanNumber;
        protected string _name;
        protected string _addressLineOne;
        protected string _addressLineTwo;
        protected string _addressCity;
        protected string _addressState;
        protected int _addressZip;
        protected int _addressZipFour;
        protected string _areaCode;
        protected string _phoneNumber;
        protected string _phoneExchange;

        #endregion

        #region <Public accessors>

        public DbContextEnums.AgencyNumber Agency
        {
            get => (_agency);
            set => _agency = value;
        }

        public string JoinedPrimaryKey
        {
            get => ($"{_accountNumber}||{_loanNumber}||{_loanSuffix}");
        }

        public int AccountNumber
        {
            get => (_accountNumber);
            set => _accountNumber = value;
        }

        public int LoanSuffix
        {
            get => (_loanSuffix);
            set => _loanSuffix = value;
        }

        public string LoanNumber
        {
            get => (_loanNumber);
            set => _loanNumber = value;
        }

        public string Name
        {
            get => (_name);
            set => _name = value;
        }

        public string PhoneExchange
        {
            get => (_phoneExchange);
            set => _phoneExchange = value;
        }

        public string PhoneNumber
        {
            get => (_phoneNumber);
            set => _phoneNumber = value;
        }

        public string AreaCode
        {
            get => (_areaCode);
            set => _areaCode = value;
        }

        public string AddressLineOne
        {
            get => (_addressLineOne);
            set => _addressLineOne = value;
        }

        public string AddressLineTwo
        {
            get => (_addressLineTwo);
            set => _addressLineTwo = value;
        }
        
        public string AddressCity
        {
            get => (_addressCity);
            set => _addressCity = value;
        }

        public string AddressState
        {
            get => (_addressState);
            set => _addressState = value;
        }

        public int AddressZip
        {
            get => (_addressZip);
            set => _addressZip = value;
        }

        public int AddressZipFour
        {
            get => (_addressZipFour);
            set => _addressZipFour = value;
        }

        #endregion
    }
}
