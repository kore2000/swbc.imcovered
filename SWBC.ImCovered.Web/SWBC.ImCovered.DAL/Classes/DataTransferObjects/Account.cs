﻿using System;
using System.Collections.Generic;

namespace SWBC.ImCovered.DAL.Classes.DataTransferObjects
{
    [Serializable]
    public class Account
    {
        #region <Private members>

        private int _id;
        private string _refNumber;
        private int _cobrandId;
        private int _agencyId;

        private ICollection<Brand> _coBrands;
        private Agency _agency;

        #endregion

        #region <Public accessors>

        public int Id
        {
            get => (_id);
            set => _id = value;
        }

        public string RefNumber
        {
            get => (_refNumber);
            set => _refNumber = value;
        }

        public int CoBrandId
        {
            get => (_cobrandId);
            set => _cobrandId = value;
        }

        public int AgencyId
        {
            get => (_agencyId);
            set => _agencyId = value;
        }

        public ICollection<Brand> CoBrands
        {
            get => (_coBrands);
            set => _coBrands = value;
        }

        public Agency Agency
        {
            get => (_agency);
            set => _agency = value;
        }

        #endregion
    }
}
