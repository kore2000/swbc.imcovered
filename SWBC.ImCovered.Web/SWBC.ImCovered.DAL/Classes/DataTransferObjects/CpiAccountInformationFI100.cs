﻿using System;

namespace SWBC.ImCovered.DAL.Classes.DataTransferObjects
{
    [Serializable]
    public class CpiAccountInformationFI100:
        BaseAccountInformation
    {
        #region <Private members>

        private CpiAccountInformationFI104 _fi104;

        #endregion

        #region <Public accessors>

        public CpiAccountInformationFI104 FI104
        {
            get => (_fi104);
            set => _fi104 = value;
        }

        #endregion
    }
}
