﻿using System;

using SWBC.ImCovered.Enums.Classes;

namespace SWBC.ImCovered.DAL.Classes.DataTransferObjects
{
    [Serializable]
    public class MortgageAccountInformationFMI104
    {
        #region <Private members>

        private DbContextEnums.AgencyNumber _agency;
        private int _accountNumber;
        private int _loanSuffix;
        private string _loanNumber;
        private char _coverageType;
        private double _referenceNumber;

        #endregion

        #region <Public accessors>

        public DbContextEnums.AgencyNumber Agency
        {
            get => (_agency);
            set => _agency = value;
        }

        public string JoinedPrimaryKey
        {
            get => ($"{_accountNumber}||{_loanNumber}||{_loanSuffix}||{_coverageType}");
        }

        public int AccountNumber
        {
            get => (_accountNumber);
            set => _accountNumber = value;
        }

        public int LoanSuffix
        {
            get => (_loanSuffix);
            set => _loanSuffix = value;
        }

        public string LoanNumber
        {
            get => (_loanNumber);
            set => _loanNumber = value;
        }

        public char CoverageType
        {
            get => (_coverageType);
            set => _coverageType = value;
        }

        public double ReferenceNumber
        {
            get => (_referenceNumber);
            set => _referenceNumber = value;
        }

        #endregion
    }
}
