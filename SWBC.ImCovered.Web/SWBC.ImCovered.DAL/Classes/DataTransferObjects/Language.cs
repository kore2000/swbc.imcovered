﻿using System;
using System.Collections.Generic;

namespace SWBC.ImCovered.DAL.Classes.DataTransferObjects
{
    [Serializable]
    public class Language
    {
        #region <Private members>

        private int _id;
        private string _name;
        private string _cultureCode;

        private ICollection<Brand> _coBrands;

        #endregion

        #region <Public accessors>

        public int Id
        {
            get => (_id);
            set => _id = value;
        }

        public string Name
        {
            get => (_name);
            set => _name = value;
        }

        public string CultureCode
        {
            get => (_cultureCode);
            set => _cultureCode = value;
        }

        public ICollection<Brand> CoBrands
        {
            get => (_coBrands);
            set => _coBrands = value;
        }

        #endregion
    }
}
