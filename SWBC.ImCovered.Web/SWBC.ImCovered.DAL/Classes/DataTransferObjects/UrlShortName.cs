﻿using System;

namespace SWBC.ImCovered.DAL.Classes.DataTransferObjects
{
    [Serializable]
    public class UrlShortName
    {
        #region <Private members>

        private int _id;
        private int _coBrandId;
        private string _name;
        private bool _isActive;

        private Brand _coBrand;

        #endregion

        #region <Public accessors>

        public int Id
        {
            get => (_id);
            set => _id = value;
        }

        public int CoBrandId
        {
            get => (_coBrandId);
            set => _coBrandId = value;
        }

        public string Name
        {
            get => (_name);
            set => _name = value;
        }

        public bool IsActive
        {
            get => (_isActive);
            set => _isActive = value;
        }

        public Brand CoBrand
        {
            get => (_coBrand);
            set => _coBrand = value;
        }

        #endregion
    }
}
