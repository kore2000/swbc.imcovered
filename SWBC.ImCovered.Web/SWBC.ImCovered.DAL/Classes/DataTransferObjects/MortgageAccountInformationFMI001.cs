﻿using System;
using System.Collections.Generic;

namespace SWBC.ImCovered.DAL.Classes.DataTransferObjects
{
    [Serializable]
    public class MortgageAccountInformationFMI001:
        BaseAccountInformation
    {
        #region <Private members>

        private string _insuredPropertyAddressLineOne;
        private string _insuredPropertyAddressLineTwo;
        private string _insuredPropertyAddressCity;
        private string _insuredPropertyAddressState;
        private int _insuredPropertyAddressZip;
        private int _insuredPropertyAddressZipFour;

        private IEnumerable<MortgageAccountInformationFMI104> _fmi104;

        #endregion

        #region <Public accessors>

        public string InsuredPropertyAddressLineOne
        {
            get => (_insuredPropertyAddressLineOne);
            set => _insuredPropertyAddressLineOne = value;
        }

        public string InsuredPropertyAddressLineTwo
        {
            get => (_insuredPropertyAddressLineTwo);
            set => _insuredPropertyAddressLineTwo = value;
        }

        public string InsuredPropertyAddressCity
        {
            get => (_insuredPropertyAddressCity);
            set => _insuredPropertyAddressCity = value;
        }

        public string InsuredPropertyAddressState
        {
            get => (_insuredPropertyAddressState);
            set => _insuredPropertyAddressState = value;
        }

        public int InsuredPropertyAddressZip
        {
            get => (_insuredPropertyAddressZip);
            set => _insuredPropertyAddressZip = value;
        }

        public int InsuredPropertyAddressZipFour
        {
            get => (_insuredPropertyAddressZipFour);
            set => _insuredPropertyAddressZipFour = value;
        }

        public IEnumerable<MortgageAccountInformationFMI104> FMI104
        {
            get => (_fmi104);
            set => _fmi104 = value;
        }

        #endregion
    }
}
