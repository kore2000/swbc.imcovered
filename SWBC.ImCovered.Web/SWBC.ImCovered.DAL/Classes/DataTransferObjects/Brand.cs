﻿using System;

namespace SWBC.ImCovered.DAL.Classes.DataTransferObjects
{
    [Serializable]
    public class Brand
    {
        #region <Private members>

        private int _id;
        private int _languageId;
        private int? _parentAccountId;
        private string _name;
        private string _phoneNumber;
        private string _faxNumber;
        private string _logoImagePath;
        private string _websiteUrl;
        private string _emailAddress;
        private string _institutionName;
        private string _mailingAddressLineOne;
        private string _mailingAddressLineTwo;
        private string _mailingAddressCity;
        private string _mailingAddressState;
        private string _mailingAddressZip;
        private bool _isActive;

        private Account _parentAccount;
        private Language _language;
        private UrlShortName _urlShortName;

        #endregion

        #region <Public accessors>

        public int Id
        {
            get => (_id);
            set => _id = value;
        }

        public int LanguageId
        {
            get => (_languageId);
            set => _languageId = value;
        }

        public int? ParentAccountId
        {
            get => (_parentAccountId);
            set => _parentAccountId = value;
        }

        public string Name
        {
            get => (_name);
            set => _name = value;
        }

        public string PhoneNumber
        {
            get => (_phoneNumber);
            set => _phoneNumber = value;
        }

        public string FaxNumber
        {
            get => (_faxNumber);
            set => _faxNumber = value;
        }

        public string LogoImagePath
        {
            get => (_logoImagePath);
            set => _logoImagePath = value;
        }

        public string WebsiteUrl
        {
            get => (_websiteUrl);
            set => _websiteUrl = value;
        }

        public string EmailAddress
        {
            get => (_emailAddress);
            set => _emailAddress = value;
        }

        public string InstitutionName
        {
            get => (_institutionName);
            set => _institutionName = value;
        }

        public string MailingAddressLineOne
        {
            get => (_mailingAddressLineOne);
            set => _mailingAddressLineOne = value;
        }

        public string MailingAddressLineTwo
        {
            get => (_mailingAddressLineTwo);
            set => _mailingAddressLineTwo = value;
        }
        
        public string MailingAddressCity
        {
            get => (_mailingAddressCity);
            set => _mailingAddressCity = value;
        }

        public string MailingAddressState
        {
            get => (_mailingAddressState);
            set => _mailingAddressState = value;
        }

        public string MailingAddressZip
        {
            get => (_mailingAddressZip);
            set => _mailingAddressZip = value;
        }
        
        public bool IsActive
        {
            get => (_isActive);
            set => _isActive = value;
        }

        public Account ParentAccount
        {
            get => (_parentAccount);
            set => _parentAccount = value;
        }

        public Language Language
        {
            get => (_language);
            set => _language = value;
        }

        public UrlShortName UrlShortName
        {
            get => (_urlShortName);
            set => _urlShortName = value;
        }

        #endregion
    }
}
