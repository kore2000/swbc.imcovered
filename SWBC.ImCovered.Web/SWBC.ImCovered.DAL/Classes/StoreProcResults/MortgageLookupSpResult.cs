﻿using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

using SWBC.ImCovered.Enums.Classes;
using SWBC.ImCovered.DAL.Classes.DataTransferObjects;

namespace SWBC.ImCovered.DAL.Classes.StoreProcResults
{
    [Serializable]
    public class MortgageLookupSpResult
    {
        #region <Private members>

        private MortgageAccountInformationFMI001 _fmi001;
        private MortgageAccountInformationFMI104 _fmi104;

        #endregion

        #region <Public accessors>

        public char CoverageType
        {
            get => (_fmi104.CoverageType);
            set => _fmi104.CoverageType = value;
        }

        public string ReferenceNumber
        {
            get => (_fmi104.ReferenceNumber.ToString());
            set => _fmi104.ReferenceNumber = (double.TryParse(
                value,
                out double convertedRefNum)) ?
                    convertedRefNum : double.MinValue;
        }

        public int AccountNumber
        {
            get => (_fmi001.AccountNumber);
            set => _fmi001.AccountNumber = value;
        }

        public int LoanSuffix
        {
            get => (_fmi001.LoanSuffix);
            set => _fmi001.LoanSuffix = value;
        }

        public string LoanNumber
        {
            get => (_fmi001.LoanNumber);
            set => _fmi001.LoanNumber = value;
        }

        public string Name
        {
            get => (_fmi001.Name);
            set => _fmi001.Name = value;
        }

        public string AddressLineOne
        {
            get => (_fmi001.AddressLineOne);
            set => _fmi001.AddressLineOne = value;
        }

        public string AddressCity
        {
            get => (_fmi001.AddressCity);
            set => _fmi001.AddressCity = value;
        }

        public string AddressState
        {
            get => (_fmi001.AddressState);
            set => _fmi001.AddressState = value;
        }

        public int AddressZip
        {
            get => (_fmi001.AddressZip);
            set => _fmi001.AddressZip = value;
        }

        public string InsuredPropertyAddressLineOne
        {
            get => (_fmi001.InsuredPropertyAddressLineOne);
            set => _fmi001.InsuredPropertyAddressLineOne = value;
        }

        public string InsuredPropertyAddressLineTwo
        {
            get => (_fmi001.InsuredPropertyAddressLineTwo);
            set => _fmi001.InsuredPropertyAddressLineTwo = value;
        }

        public string InsuredPropertyAddressCity
        {
            get => (_fmi001.InsuredPropertyAddressCity);
            set => _fmi001.InsuredPropertyAddressCity = value;
        }

        public string InsuredPropertyAddressState
        {
            get => (_fmi001.InsuredPropertyAddressState);
            set => _fmi001.InsuredPropertyAddressState = value;
        }

        public int InsuredPropertyAddressZip
        {
            get => (_fmi001.InsuredPropertyAddressZip);
            set => _fmi001.InsuredPropertyAddressZip = value;
        }

        public MortgageAccountInformationFMI001 FMI001
        {
            get => (_fmi001);
            set => _fmi001 = value;
        }

        public MortgageAccountInformationFMI104 FMI104
        {
            get => (_fmi104);
            set => _fmi104 = value;
        }

        #endregion

        #region <Constructors>

        public MortgageLookupSpResult()
        {
            if (_fmi001 == null)
            {
                _fmi001 = new MortgageAccountInformationFMI001();
            }
            if (_fmi104 == null)
            {
                _fmi104 = new MortgageAccountInformationFMI104();
            }
        }

        #endregion
    }
}
