﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;
using SWBC.ImCovered.DAL.Classes.StoreProcResults;

namespace SWBC.ImCovered.DAL.Classes.Mapping
{
    public class MortgageLookupSpResultQueryConfig :
        IQueryTypeConfiguration<MortgageLookupSpResult>
    {
        public void Configure(
            QueryTypeBuilder<MortgageLookupSpResult> pBuilder)
        {
            pBuilder
                .Ignore(t => t.FMI001)
                .Ignore(t => t.FMI104);

            pBuilder
                .Property(t => t.ReferenceNumber)
                .HasColumnName("IV_CROSS_REF_NO");
            pBuilder
                .Property(t => t.CoverageType)
                .HasColumnName("IV_COV_TYPE")
                .IsFixedLength();
            pBuilder
                .Property(t => t.AccountNumber)
                .HasColumnName("IS_ACCOUNT_NO");
            pBuilder
                .Property(t => t.LoanSuffix)
                .HasColumnName("IS_LOAN_SUFFIX");
            pBuilder
                .Property(t => t.LoanNumber)
                .HasColumnName("IS_LOAN_NO")
                .HasMaxLength(35);
            pBuilder
                .Property(t => t.Name)
                .HasColumnName("IL_NAME")
                .HasMaxLength(30);
            pBuilder
                .Property(t => t.InsuredPropertyAddressLineOne)
                .HasColumnName("IL_ADDR_1")
                .HasMaxLength(30);
            pBuilder
                .Property(t => t.InsuredPropertyAddressLineTwo)
                .HasColumnName("IL_ADDR_2")
                .HasMaxLength(30);
            pBuilder
                .Property(t => t.InsuredPropertyAddressCity)
                .HasColumnName("IL_CITY")
                .HasMaxLength(25);
            pBuilder
                .Property(t => t.InsuredPropertyAddressState)
                .HasColumnName("IL_STATE")
                .HasMaxLength(2);
            pBuilder
                .Property(t => t.InsuredPropertyAddressZip)
                .HasColumnName("IL_ZIP");
            pBuilder
                .Property(t => t.AddressLineOne)
                .HasColumnName("IL_USER_ADDR_1")
                .HasMaxLength(30);
            pBuilder
                .Property(t => t.AddressCity)
                .HasColumnName("IL_USER_CITY")
                .HasMaxLength(25);
            pBuilder
                .Property(t => t.AddressState)
                .HasColumnName("IL_USER_STATE")
                .HasMaxLength(2);
            pBuilder
                .Property(t => t.AddressZip)
                .HasColumnName("IL_USER_ZIP");

        }
    }
}
