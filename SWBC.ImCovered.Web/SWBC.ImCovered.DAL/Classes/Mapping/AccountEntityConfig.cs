﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;

namespace SWBC.ImCovered.DAL.Classes.Mapping
{
    public class AccountEntityConfig :
        IEntityTypeConfiguration<Account>
    {
        public void Configure(
            EntityTypeBuilder<Account> pBuilder)
        {
            pBuilder
                .ToTable("tbl_Accounts");

            pBuilder
                .HasKey(t => t.Id);
            pBuilder
                .Property(t => t.Id)
                .HasColumnName("ID")
                .IsRequired();

            pBuilder
                .HasOne(t => t.Agency)
                .WithMany(t => t.Accounts)
                .HasForeignKey(t => t.AgencyId);
            pBuilder
                .Property(t => t.AgencyId)
                .HasColumnName("AgencyID")
                .IsRequired();

            pBuilder
                .Property(t => t.CoBrandId)
                .HasColumnName("CoBrandID")
                .IsRequired();
            pBuilder
                .Property(t => t.RefNumber)
                .HasColumnName("RefNum")
                .IsRequired()
                .HasMaxLength(10);
        }
    }
}
