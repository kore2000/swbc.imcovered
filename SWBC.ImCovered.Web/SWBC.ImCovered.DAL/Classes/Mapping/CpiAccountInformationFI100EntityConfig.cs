﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;

namespace SWBC.ImCovered.DAL.Classes.Mapping
{
    public class CpiAccountInformationFI00EntityConfig :
        IEntityTypeConfiguration<CpiAccountInformationFI100>
    {
        public void Configure(
            EntityTypeBuilder<CpiAccountInformationFI100> pBuilder)
        {
            pBuilder
                .ToTable("FI100");

            pBuilder
                .HasKey(t => new { t.AccountNumber, t.LoanSuffix, t.LoanNumber });
            pBuilder
                .Property(t => t.AccountNumber)
                .HasColumnName("IS_ACCOUNT_NO")
                .IsRequired();
            pBuilder
                .Property(t => t.LoanSuffix)
                .HasColumnName("IS_LOAN_SUFFIX")
                .IsRequired();
            pBuilder
                .Property(t => t.LoanNumber)
                .HasColumnName("IS_LOAN_NO")
                .IsRequired()
                .HasMaxLength(15);

            pBuilder
                .Ignore(t => t.Agency)
                .Ignore(t => t.JoinedPrimaryKey)
                .Ignore(t => t.FI104);

            pBuilder
                .Property(t => t.Name)
                .HasColumnName("IL_NAME")
                .HasMaxLength(30);
            pBuilder
                .Property(t => t.AreaCode)
                .HasColumnName("IL_AREA_CODE")
                .HasMaxLength(3);
            pBuilder
                .Property(t => t.PhoneExchange)
                .HasColumnName("IL_EXCHANGE")
                .HasMaxLength(3);
            pBuilder
                .Property(t => t.PhoneNumber)
                .HasColumnName("IL_PHONE_NO")
                .HasMaxLength(4);
            pBuilder
                .Property(t => t.AddressLineOne)
                .HasColumnName("IL_ADDR_1")
                .HasMaxLength(30);
            pBuilder
                .Property(t => t.AddressLineTwo)
                .HasColumnName("IL_ADDR_2")
                .HasMaxLength(30);
            pBuilder
                .Property(t => t.AddressCity)
                .HasColumnName("IL_CITY")
                .HasMaxLength(25);
            pBuilder
                .Property(t => t.AddressState)
                .HasColumnName("IL_STATE")
                .HasMaxLength(2);
            pBuilder
                .Property(t => t.AddressZip)
                .HasColumnName("IL_ZIP");
            pBuilder
                .Property(t => t.AddressZipFour)
                .HasColumnName("IL_ZIP_4");
        }
    }
}
