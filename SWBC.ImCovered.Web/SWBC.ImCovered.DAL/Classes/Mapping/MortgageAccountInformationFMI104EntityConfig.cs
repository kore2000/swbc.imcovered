﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;

namespace SWBC.ImCovered.DAL.Classes.Mapping
{
    public class MortgageAccountInformationFMI104EntityConfig :
        IEntityTypeConfiguration<MortgageAccountInformationFMI104>
    {
        public void Configure(
            EntityTypeBuilder<MortgageAccountInformationFMI104> pBuilder)
        {
            pBuilder
                .ToTable("FMI104");

            pBuilder
                .HasKey(t => new { t.AccountNumber, t.LoanSuffix, t.LoanNumber, t.CoverageType });
            pBuilder
                .Property(t => t.AccountNumber)
                .HasColumnName("IV_ACCOUNT_NO")
                .IsRequired();
            pBuilder
                .Property(t => t.LoanSuffix)
                .HasColumnName("IV_LOAN_SUFFIX")
                .IsRequired();
            pBuilder
                .Property(t => t.LoanNumber)
                .HasColumnName("IV_LOAN_NO")
                .IsRequired()
                .HasMaxLength(35);
            pBuilder
                .Property(t => t.CoverageType)
                .HasColumnName("IV_COV_TYPE")
                .IsFixedLength()
                .IsRequired();

            pBuilder
                .Ignore(t => t.Agency)
                .Ignore(t => t.JoinedPrimaryKey);

            pBuilder
                .Property(t => t.ReferenceNumber)
                .HasColumnName("IV_CROSS_REF_NO")
                .IsRequired();
        }
    }
}
