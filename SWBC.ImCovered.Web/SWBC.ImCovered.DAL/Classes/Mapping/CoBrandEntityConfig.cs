﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;

namespace SWBC.ImCovered.DAL.Classes.Mapping
{
    public class CoBrandEntityConfig :
        IEntityTypeConfiguration<Brand>
    {
        public void Configure(
            EntityTypeBuilder<Brand> pBuilder)
        {
            pBuilder
                .ToTable("tbl_CoBrands");

            pBuilder
                .HasKey(t => t.Id);
            pBuilder
                .Property(t => t.Id)
                .HasColumnName("ID")
                .IsRequired();

            pBuilder
                .HasOne(t => t.ParentAccount)
                .WithMany(t => t.CoBrands)
                .HasForeignKey(t => t.ParentAccountId);
            pBuilder
                .Property(t => t.ParentAccountId)
                .HasColumnName("ParentAccountID");

            pBuilder
                .HasOne(t => t.Language)
                .WithMany(t => t.CoBrands)
                .HasForeignKey(t => t.LanguageId);
            pBuilder
                .Property(t => t.LanguageId)
                .HasColumnName("LanguageID")
                .IsRequired();

            pBuilder
                .Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(30);
            pBuilder
                .Property(t => t.PhoneNumber)
                .HasColumnName("PhoneNumber")
                .IsRequired()
                .HasMaxLength(20);
            pBuilder
                .Property(t => t.FaxNumber)
                .HasColumnName("FaxNumber")
                .HasMaxLength(50);
            pBuilder
                .Property(t => t.LogoImagePath)
                .HasColumnName("LogoImage")
                .HasMaxLength(50);
            pBuilder
                .Property(t => t.WebsiteUrl)
                .HasColumnName("Website")
                .HasMaxLength(100);
            pBuilder
                .Property(t => t.EmailAddress)
                .HasColumnName("EmailAddress")
                .HasMaxLength(50);
            pBuilder
                .Property(t => t.IsActive)
                .HasColumnName("isActive")
                .IsRequired();
            pBuilder
                .Property(t => t.InstitutionName)
                .HasColumnName("InstName")
                .IsRequired()
                .HasMaxLength(100);
            pBuilder
                .Property(t => t.MailingAddressLineOne)
                .HasColumnName("MailingAddressLine1")
                .HasMaxLength(50);
            pBuilder
                .Property(t => t.MailingAddressLineTwo)
                .HasColumnName("MailingAddressLine2")
                .HasMaxLength(50);
            pBuilder
                .Property(t => t.MailingAddressCity)
                .HasColumnName("MailingAddressCity")
                .HasMaxLength(50);
            pBuilder
                .Property(t => t.MailingAddressState)
                .HasColumnName("MailingAddressState")
                .HasMaxLength(2);
            pBuilder
                .Property(t => t.MailingAddressZip)
                .HasColumnName("MailingAddressZip")
                .HasMaxLength(5);
        }
    }
}
