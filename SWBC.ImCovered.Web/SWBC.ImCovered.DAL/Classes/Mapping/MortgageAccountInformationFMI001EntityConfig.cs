﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;

namespace SWBC.ImCovered.DAL.Classes.Mapping
{
    public class MortgageAccountInformationFMI001EntityConfig :
        IEntityTypeConfiguration<MortgageAccountInformationFMI001>
    {
        public void Configure(
            EntityTypeBuilder<MortgageAccountInformationFMI001> pBuilder)
        {
            pBuilder
                .ToTable("FMI001");

            pBuilder
                .HasKey(t => new { t.AccountNumber, t.LoanSuffix, t.LoanNumber });
            pBuilder
                .Property(t => t.AccountNumber)
                .HasColumnName("IC_ACCOUNT_NO")
                .IsRequired();
            pBuilder
                .Property(t => t.LoanSuffix)
                .HasColumnName("IC_LOAN_SUFFIX")
                .IsRequired();
            pBuilder
                .Property(t => t.LoanNumber)
                .HasColumnName("IC_LOAN_NO")
                .IsRequired()
                .HasMaxLength(35);

            pBuilder
                .Ignore(t => t.Agency)
                .Ignore(t => t.JoinedPrimaryKey)
                .Ignore(t => t.FMI104);

            pBuilder
                .Property(t => t.Name)
                .HasColumnName("IL_NAME")
                .HasMaxLength(30);
            pBuilder
                .Property(t => t.AreaCode)
                .HasColumnName("IL_AREA_CODE")
                .HasMaxLength(3)
                .IsRequired();
            pBuilder
                .Property(t => t.PhoneExchange)
                .HasColumnName("IL_EXCHANGE")
                .HasMaxLength(3)
                .IsRequired();
            pBuilder
                .Property(t => t.PhoneNumber)
                .HasColumnName("IL_PHONE_NO")
                .HasMaxLength(4)
                .IsRequired();
            pBuilder
                .Property(t => t.AddressLineOne)
                .HasColumnName("IL_ADDR_1")
                .HasMaxLength(30)
                .IsRequired();
            pBuilder
                .Property(t => t.AddressLineTwo)
                .HasColumnName("IL_ADDR_2")
                .HasMaxLength(30)
                .IsRequired();
            pBuilder
                .Property(t => t.AddressCity)
                .HasColumnName("IL_CITY")
                .HasMaxLength(25)
                .IsRequired();
            pBuilder
                .Property(t => t.AddressState)
                .HasColumnName("IL_STATE")
                .HasMaxLength(2)
                .IsRequired();
            pBuilder
                .Property(t => t.AddressZip)
                .HasColumnName("IL_ZIP")
                .IsRequired();
            pBuilder
                .Property(t => t.AddressZipFour)
                .HasColumnName("IL_ZIP_4")
                .IsRequired();
            pBuilder
                .Property(t => t.InsuredPropertyAddressLineOne)
                .HasColumnName("IC_PROP_ADDR_1")
                .HasMaxLength(30)
                .IsRequired();
            pBuilder
                .Property(t => t.InsuredPropertyAddressLineTwo)
                .HasColumnName("IC_PROP_ADDR_2")
                .HasMaxLength(30)
                .IsRequired();
            pBuilder
                .Property(t => t.InsuredPropertyAddressCity)
                .HasColumnName("IC_PROP_CITY")
                .HasMaxLength(25)
                .IsRequired();
            pBuilder
                .Property(t => t.InsuredPropertyAddressState)
                .HasColumnName("IC_PROP_STATE")
                .HasMaxLength(2)
                .IsRequired();
            pBuilder
                .Property(t => t.InsuredPropertyAddressZip)
                .HasColumnName("IC_PROP_ZIP")
                .IsRequired();
            pBuilder
                .Property(t => t.InsuredPropertyAddressZipFour)
                .HasColumnName("IC_PROP_ZIP_4")
                .IsRequired();
        }
    }
}
