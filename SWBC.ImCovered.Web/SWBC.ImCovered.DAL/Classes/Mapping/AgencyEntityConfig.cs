﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;

namespace SWBC.ImCovered.DAL.Classes.Mapping
{
    public class AgencyEntityConfig :
        IEntityTypeConfiguration<Agency>
    {
        public void Configure(
            EntityTypeBuilder<Agency> pBuilder)
        {
            pBuilder
                .ToTable("tbl_Agency");

            pBuilder
                .HasKey(t => t.Id);

            pBuilder
                .Property(t => t.Id)
                .HasColumnName("ID");
            pBuilder
                .Property(t => t.Name)
                .HasColumnName("Name")
                .IsRequired()
                .HasMaxLength(20);
        }
    }
}
