﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;

namespace SWBC.ImCovered.DAL.Classes.Mapping
{
    public class UrlShortNameEntityConfig :
        IEntityTypeConfiguration<UrlShortName>
    {
        public void Configure(
            EntityTypeBuilder<UrlShortName> pBuilder)
        {
            pBuilder
                .ToTable("tbl_URLShortNames");

            pBuilder
                .HasKey(t => t.Id);
            pBuilder
                .Property(t => t.Id)
                .HasColumnName("ID");

            pBuilder
                .HasOne(t => t.CoBrand)
                .WithOne(t => t.UrlShortName)
                .HasForeignKey<UrlShortName>(t => t.CoBrandId);
            pBuilder
                .Property(t => t.CoBrandId)
                .HasColumnName("CoBrandID")
                .IsRequired();

            pBuilder
                .Property(t => t.Name)
                .HasColumnName("URLShortName")
                .IsRequired()
                .HasMaxLength(50);
            pBuilder
                .Property(t => t.IsActive)
                .HasColumnName("Active")
                .IsRequired();
        }
    }
}
