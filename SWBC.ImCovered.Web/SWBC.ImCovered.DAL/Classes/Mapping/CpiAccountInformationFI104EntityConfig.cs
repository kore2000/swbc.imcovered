﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;

namespace SWBC.ImCovered.DAL.Classes.Mapping
{
    public class CpiAccountInformationFI104EntityConfig :
        IEntityTypeConfiguration<CpiAccountInformationFI104>
    {
        public void Configure(
            EntityTypeBuilder<CpiAccountInformationFI104> pBuilder)
        {
            pBuilder
                .ToTable("FI104");

            pBuilder
                .HasKey(t => new { t.AccountNumber, t.LoanSuffix, t.LoanNumber });
            pBuilder
                .Property(t => t.AccountNumber)
                .HasColumnName("IV_ACCOUNT_NO")
                .IsRequired();
            pBuilder
                .Property(t => t.LoanSuffix)
                .HasColumnName("IV_LOAN_SUFFIX")
                .IsRequired();
            pBuilder
                .Property(t => t.LoanNumber)
                .HasColumnName("IV_LOAN_NO")
                .IsRequired()
                .HasMaxLength(15);

            pBuilder
                .Ignore(t => t.JoinedPrimaryKey);

            pBuilder
                .Property(t => t.ReferenceNumber)
                .HasColumnName("IV_CROSS_REF_NO");
        }
    }
}
