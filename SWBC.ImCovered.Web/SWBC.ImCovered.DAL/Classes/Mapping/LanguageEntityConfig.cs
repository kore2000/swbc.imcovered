﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;

namespace SWBC.ImCovered.DAL.Classes.Mapping
{
    public class LanguageEntityConfig :
        IEntityTypeConfiguration<Language>
    {
        public void Configure(
            EntityTypeBuilder<Language> pBuilder)
        {
            pBuilder
                .ToTable("tbl_Languages");

            pBuilder
                .HasKey(t => t.Id);
            pBuilder
                .Property(t => t.Id)
                .HasColumnName("ID");

            pBuilder
                .Property(t => t.Name)
                .HasColumnName("Name")
                .IsRequired()
                .HasMaxLength(50);
            pBuilder
                .Property(t => t.CultureCode)
                .HasColumnName("Culture")
                .IsRequired()
                .HasMaxLength(10);
        }
    }
}
