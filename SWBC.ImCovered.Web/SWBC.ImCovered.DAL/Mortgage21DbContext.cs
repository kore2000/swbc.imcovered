﻿using Microsoft.EntityFrameworkCore;

using SWBC.ImCovered.Enums.Classes;

namespace SWBC.ImCovered.DAL
{
    public class Mortgage21DbContext :
        BaseMortgageDbContext
    {
        #region <Constructors>

        public Mortgage21DbContext()
        {
        }

        public Mortgage21DbContext(
            DbContextOptions<Mortgage21DbContext> pOptions) :
            base(
                DbContextEnums.AgencyNumber.AgencyTwentyOne,
                pOptions)
        {
        }

        #endregion
    }
}
