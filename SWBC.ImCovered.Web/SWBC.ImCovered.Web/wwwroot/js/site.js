
class DataLoader {

    constructor() {
        FontAwesomeConfig.autoReplaceSvg = 'nest';
        this.registerAndPushCustomKOLoader();
    }

    loginData(pDataObj) {
        try {
            let passedData = pDataObj || {};
            if (!passedData) {
                this.logToConsole(
                    "failed to retrieve login info");
                return;
            }
            this.attachLoginToKO(passedData);
        } catch (pEx) {
            this.logToConsole(pEx.message);
        }
    }

    dashboardData(pApiUrl) {
        $.ajax({
            dataType: "json",
            url: pApiUrl,
            context: this,
            success: function (pData) {
                try {
                    if (!pData) {
                        this.logToConsole(
                            "failed to retrieve dashboard info");
                        return;
                    }
                    this.attachDashboardToKO(pData);
                } catch (pExInner) {
                    this.logToConsole(pExInner.message);
                }
            },
            fail: function () {
                this.logToConsole(
                    "failed to load login info");
            }
        });
    }

    attachLoginToKO(pDataObj) {
        ko.components.register('login-input-box', {
            viewModel: LoginPageInputBoxViewModel,
            template: {
                templateUrl: LoginPageInputBoxViewModel.getTemplate(
                    pDataObj.baseUrl)
            }
        });
        ko.components.register('error-summary', {
            viewModel: ErrorSummaryViewModel,
            template: {
                templateUrl: ErrorSummaryViewModel.getTemplate(
                    pDataObj.baseUrl)
            }
        });

        ko.applyBindings(
            new LoginViewModel(pDataObj));
    }

    attachDashboardToKO(pDataObj) {
        ko.applyBindings(
            new DashboardViewModel(pDataObj));
    }

    logToConsole(pMessage) {
        console.log(`An error has occurred: ${pMessage}`);
    }

    registerAndPushCustomKOLoader() {
        var templateFromUrl = {
            loadTemplate: function (
                pName,
                pTemplateConfig,
                pCallback) {
                if (pTemplateConfig.templateUrl) {                    
                    jQuery.get(
                        pTemplateConfig.templateUrl,
                        function (pResponse) {
                            ko.components.defaultLoader.loadTemplate(
                                pName, pResponse, pCallback);
                    });
                } else {
                    callback(null);
                }
            }
        };

        ko.components.loaders.unshift(
            templateFromUrl);
    }
}
