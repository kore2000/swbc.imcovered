"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DataLoader = function () {
    function DataLoader() {
        _classCallCheck(this, DataLoader);

        FontAwesomeConfig.autoReplaceSvg = 'nest';
        this.registerAndPushCustomKOLoader();
    }

    _createClass(DataLoader, [{
        key: "loginData",
        value: function loginData(pDataObj) {
            try {
                var passedData = pDataObj || {};
                if (!passedData) {
                    this.logToConsole("failed to retrieve login info");
                    return;
                }
                this.attachLoginToKO(passedData);
            } catch (pEx) {
                this.logToConsole(pEx.message);
            }
        }
    }, {
        key: "dashboardData",
        value: function dashboardData(pApiUrl) {
            $.ajax({
                dataType: "json",
                url: pApiUrl,
                context: this,
                success: function success(pData) {
                    try {
                        if (!pData) {
                            this.logToConsole("failed to retrieve dashboard info");
                            return;
                        }
                        this.attachDashboardToKO(pData);
                    } catch (pExInner) {
                        this.logToConsole(pExInner.message);
                    }
                },
                fail: function fail() {
                    this.logToConsole("failed to load login info");
                }
            });
        }
    }, {
        key: "attachLoginToKO",
        value: function attachLoginToKO(pDataObj) {
            ko.components.register('login-input-box', {
                viewModel: LoginPageInputBoxViewModel,
                template: {
                    templateUrl: LoginPageInputBoxViewModel.getTemplate(pDataObj.baseUrl)
                }
            });
            ko.components.register('error-summary', {
                viewModel: ErrorSummaryViewModel,
                template: {
                    templateUrl: ErrorSummaryViewModel.getTemplate(pDataObj.baseUrl)
                }
            });

            ko.applyBindings(new LoginViewModel(pDataObj));
        }
    }, {
        key: "attachDashboardToKO",
        value: function attachDashboardToKO(pDataObj) {
            ko.applyBindings(new DashboardViewModel(pDataObj));
        }
    }, {
        key: "logToConsole",
        value: function logToConsole(pMessage) {
            console.log("An error has occurred: " + pMessage);
        }
    }, {
        key: "registerAndPushCustomKOLoader",
        value: function registerAndPushCustomKOLoader() {
            var templateFromUrl = {
                loadTemplate: function loadTemplate(pName, pTemplateConfig, pCallback) {
                    if (pTemplateConfig.templateUrl) {
                        jQuery.get(pTemplateConfig.templateUrl, function (pResponse) {
                            ko.components.defaultLoader.loadTemplate(pName, pResponse, pCallback);
                        });
                    } else {
                        callback(null);
                    }
                }
            };

            ko.components.loaders.unshift(templateFromUrl);
        }
    }]);

    return DataLoader;
}();

var DashboardViewModel = function DashboardViewModel(pDataObj) {
    _classCallCheck(this, DashboardViewModel);

    this.uploadonly = ko.observable(pDataObj.uploadOnly || true);
    this.insuranceStatus = ko.observable(pDataObj.insuranceStatus || null);
    this.friendlyStatus = ko.observable(pDataObj.friendlyStatus || null);
    this.insuranceStatusClass = ko.observable(pDataObj.insuranceStatusClass || null);
    this.firstName = ko.observable(pDataObj.firstName || null);

    this.displayUploadOnly = ko.pureComputed(function () {
        return this.uploadonly ? "d-none" : "";
    }, this);
};

var ErrorSummaryViewModel = function () {
    function ErrorSummaryViewModel(pDataObj) {
        _classCallCheck(this, ErrorSummaryViewModel);

        pDataObj = pDataObj || {};

        this.errorArray = ko.observableArray(pDataObj.errorArray || []);
    }

    _createClass(ErrorSummaryViewModel, [{
        key: "addErrors",
        value: function addErrors(pErrorArray) {
            this.errorArray(pErrorArray);
        }
    }, {
        key: "errorsPresentCheck",
        value: function errorsPresentCheck() {
            return this.errorArray().length > 0 ? "collapse.show" : "collapse";
        }
    }], [{
        key: "getTemplate",
        value: function getTemplate(pBaseUrl) {
            pBaseUrl = !pBaseUrl || !pBaseUrl.trim() ? '/' : pBaseUrl;
            var templatePath = "js/components/errorSummary.html";

            return "" + pBaseUrl + templatePath;
        }
    }]);

    return ErrorSummaryViewModel;
}();

var LoginPageInputBoxViewModel = function () {
    function LoginPageInputBoxViewModel(pDataObj) {
        _classCallCheck(this, LoginPageInputBoxViewModel);

        pDataObj = pDataObj || {};

        this.requiredErrorMessage = ko.observable(pDataObj.requiredErrorMessage || null);
        this.invalidErrorMessage = ko.observable(pDataObj.invalidErrorMessage || null);
        this.inputLabel = ko.observable(pDataObj.inputLabel || null);
        this.inputValue = ko.observable(pDataObj.inputValue || null);
        this.inputId = ko.observable(pDataObj.inputId || null);
        this.inputAttr = ko.observable(this.getInputAttr(pDataObj.inputAttr, pDataObj.inputId));
        this.inputFormat = pDataObj.inputFormat || null;

        this.isEmpty = ko.pureComputed(this.isEmptyCheck, this);
        this.isInvalid = ko.pureComputed(this.isInvalidCheck, this);
        this.isInputValid = ko.pureComputed(this.isInputValidCheck, this);
    }

    _createClass(LoginPageInputBoxViewModel, [{
        key: "isInvalidCheck",
        value: function isInvalidCheck() {
            if (!this.inputFormat) {
                return "d-none";
            }
            var inputVal = ko.utils.unwrapObservable(this.inputValue);

            if (!inputVal || !inputVal.trim()) {
                return "d-none";
            }

            var tester = RegExp(this.inputFormat);
            return !tester.test(inputVal) ? "" : "d-none";
        }
    }, {
        key: "isEmptyCheck",
        value: function isEmptyCheck() {
            var inputVal = ko.utils.unwrapObservable(this.inputValue);
            return !inputVal || !inputVal.trim() ? "" : "d-none";
        }
    }, {
        key: "isInputValidCheck",
        value: function isInputValidCheck() {
            var inputVal = ko.utils.unwrapObservable(this.inputValue);

            var isEmpty = !inputVal || !inputVal.trim();
            var isInvalid = this.inputFormat && !RegExp(this.inputFormat).test(inputVal);

            if (isEmpty || isInvalid) {
                if (isEmpty) {
                    return "input-validation-error empty";
                }
                return "input-validation-error not-empty";
            }

            return "valid";
        }
    }, {
        key: "getInputAttr",
        value: function getInputAttr(pInputAttr, pInputId) {
            pInputAttr = pInputAttr || {};
            pInputId = pInputId || null;

            pInputAttr.id = pInputId;
            pInputAttr.name = pInputId;
            return pInputAttr;
        }
    }], [{
        key: "getTemplate",
        value: function getTemplate(pBaseUrl) {
            pBaseUrl = !pBaseUrl || !pBaseUrl.trim() ? '/' : pBaseUrl;
            var templatePath = "js/components/loginPageInputBoxComponent.html";

            return "" + pBaseUrl + templatePath;
        }
    }]);

    return LoginPageInputBoxViewModel;
}();

var LoginViewModel = function () {
    function LoginViewModel(pDataObj) {
        _classCallCheck(this, LoginViewModel);

        this.uid = ko.observable(pDataObj.uid || "");
        this.zip = ko.observable(pDataObj.zipCode || "");
        this.lastName = ko.observable(pDataObj.lastName || "");
        this.activeTab = ko.observable(pDataObj.activeTab || "uid");

        this.errors = ko.observableArray();
        this.errors.subscribe(function (pNewValue) {
            var activeTabValue = ko.utils.unwrapObservable(this.activeTab),
                activeForm = activeTabValue === "uid" ? "uidLoginPanel" : "uploadDocsPanel",
                errorSummaryDiv = document.getElementById(activeForm).getElementsByClassName("error-summary"),
                errorSummaryVm = errorSummaryDiv.length > 0 ? ko.dataFor(errorSummaryDiv[0]) : null;

            if (!errorSummaryVm) {
                console.log("Error: ViewModel is empty or null");
                return;
            }

            errorSummaryVm.addErrors(pNewValue);
            this.activeTab.notifySubscribers(activeTabValue, "change");
        }, this);

        this.uidTabIsActive = ko.pureComputed(this.uidTabIsActive, this);
        this.noUidTabIsActive = ko.pureComputed(this.noUidTabIsActive, this);
        this.uidSectionIsActive = ko.pureComputed(this.uidSectionIsActive, this);
        this.noUidSectionIsActive = ko.pureComputed(this.noUidSectionIsActive, this);
        this.errorsPresent = ko.pureComputed(this.errorsPresentCheck, this);
    }

    _createClass(LoginViewModel, [{
        key: "uidTabIsActive",
        value: function uidTabIsActive() {
            return ko.utils.unwrapObservable(this.activeTab) === "uid" ? "active" : "";
        }
    }, {
        key: "noUidTabIsActive",
        value: function noUidTabIsActive() {
            return ko.utils.unwrapObservable(this.activeTab) === "nouid" ? "active" : "";
        }
    }, {
        key: "uidSectionIsActive",
        value: function uidSectionIsActive() {
            return ko.utils.unwrapObservable(this.activeTab) === "uid" ? "collapse.show" : "collapse";
        }
    }, {
        key: "noUidSectionIsActive",
        value: function noUidSectionIsActive() {
            return ko.utils.unwrapObservable(this.activeTab) === "nouid" ? "collapse.show" : "collapse";
        }
    }, {
        key: "errorsPresentCheck",
        value: function errorsPresentCheck() {
            var activeTabValue = ko.utils.unwrapObservable(this.activeTab),
                activeForm = activeTabValue === "uid" ? "uidLoginPanel" : "uploadDocsPanel",
                errorSummaryDiv = document.getElementById(activeForm).getElementsByClassName("error-summary"),
                errorSummaryVm = errorSummaryDiv.length > 0 ? ko.dataFor(errorSummaryDiv[0]) : null;

            if (!errorSummaryVm) {
                return "collapse";
            }

            return errorSummaryVm.errorsPresentCheck();
        }
    }, {
        key: "setUidTabActive",
        value: function setUidTabActive($data, $event) {
            var target = jQuery($event.currentTarget),
                headerText = target.data('header-text'),
                allTabs = target.parent().find('.login-pane-selector');

            allTabs.text(function (pIndex) {
                var tab = jQuery(this);
                return tab.data('link-text');
            });

            target.text(headerText);
            this.activeTab("uid");
        }
    }, {
        key: "setNoUidTabActive",
        value: function setNoUidTabActive($data, $event) {
            var target = jQuery($event.currentTarget),
                headerText = target.data('header-text'),
                allTabs = target.parent().find('.login-pane-selector');

            allTabs.text(function (pIndex) {
                var tab = jQuery(this);
                return tab.data('link-text');
            });

            target.text(headerText);
            this.activeTab("nouid");
        }
    }, {
        key: "addErrors",
        value: function addErrors(pErrorArray) {
            if (!Array.isArray(pErrorArray)) {
                this.errors([]);
                this.errors.push(pErrorArray);
                return;
            }

            this.errors(pErrorArray);
        }
    }, {
        key: "validateForm",
        value: function validateForm($form) {
            event.preventDefault();
            var target = jQuery($form);

            var allInvalidInputs = target.find(".input-validation-error");
            if (allInvalidInputs.length == 0) {
                this.processForm($form);
                return true;
            }

            allInvalidInputs.each(function (pIndex, pVal) {
                var item = jQuery(pVal),
                    parentFormGroup = item.closest(".form-group");

                if (item.hasClass("empty")) {
                    var requiredTooltip = parentFormGroup.find('.require > svg');

                    requiredTooltip.tooltip({
                        container: this.parentElement
                    }).tooltip("show");
                    setTimeout(function (pTooltip) {
                        pTooltip.tooltip('hide');
                    }, 3000, requiredTooltip);
                } else if (item.hasClass("not-empty")) {
                    var invalidTooltip = parentFormGroup.find('.invalid > svg');

                    invalidTooltip.tooltip({
                        container: this.parentElement
                    }).tooltip("show");
                    setTimeout(function (pTooltip) {
                        pTooltip.tooltip('hide');
                    }, 3000, invalidTooltip);
                }
            });
        }
    }, {
        key: "processForm",
        value: function processForm($form) {
            var koContext = this;
            jQuery.ajax({
                type: "POST",
                url: $form.action,
                context: $form,
                dataType: "json",
                data: jQuery($form).serialize(),
                success: function (pData, pStatus, pXhr) {
                    if (pData.success) {
                        document.location.href = pData.redirectUrl;
                    } else {
                        this.addErrors(pData.errors);
                    }
                }.bind(koContext),
                error: function (pXhr, pStatus, pError) {
                    this.addErrors(pError.toString());
                }.bind(koContext)
            });
        }
    }]);

    return LoginViewModel;
}();