class LoginViewModel {    
    constructor(pDataObj) {
        this.uid = ko.observable(
            pDataObj.uid || "");
        this.zip = ko.observable(
            pDataObj.zipCode || "");
        this.lastName = ko.observable(
            pDataObj.lastName || "");
        this.activeTab = ko.observable(
            pDataObj.activeTab || "uid");

        this.errors = ko.observableArray();
        this.errors.subscribe(function (pNewValue) {
            let activeTabValue = ko.utils.unwrapObservable(this.activeTab),
                activeForm = activeTabValue === "uid" ?
                    "uidLoginPanel" : "uploadDocsPanel",
                errorSummaryDiv = document
                    .getElementById(activeForm)
                    .getElementsByClassName("error-summary"),
                errorSummaryVm = errorSummaryDiv.length > 0 ?
                    ko.dataFor(errorSummaryDiv[0]) : null;

            if (!errorSummaryVm) {
                console.log("Error: ViewModel is empty or null");
                return;
            }

            errorSummaryVm.addErrors(pNewValue);
            this.activeTab.notifySubscribers(activeTabValue, "change");
        }, this);

        this.uidTabIsActive = ko.pureComputed(
            this.uidTabIsActive, this);
        this.noUidTabIsActive = ko.pureComputed(
            this.noUidTabIsActive, this);
        this.uidSectionIsActive = ko.pureComputed(
            this.uidSectionIsActive, this);
        this.noUidSectionIsActive = ko.pureComputed(
            this.noUidSectionIsActive, this);
        this.errorsPresent = ko.pureComputed(
            this.errorsPresentCheck, this);
    }

    uidTabIsActive() {
        return (ko.utils.unwrapObservable(this.activeTab) === "uid" ?
            "active" : "");
    }

    noUidTabIsActive() {
        return (ko.utils.unwrapObservable(this.activeTab) === "nouid" ?
            "active" : "");
    }

    uidSectionIsActive() {
        return (ko.utils.unwrapObservable(this.activeTab) === "uid" ?
            "collapse.show" : "collapse");
    }

    noUidSectionIsActive() {
        return (ko.utils.unwrapObservable(this.activeTab) === "nouid" ?
            "collapse.show" : "collapse");
    }

    errorsPresentCheck() {
        let activeTabValue = ko.utils.unwrapObservable(this.activeTab),
            activeForm = activeTabValue === "uid" ?
                "uidLoginPanel" : "uploadDocsPanel",
            errorSummaryDiv = document
                .getElementById(activeForm)
                .getElementsByClassName("error-summary"),
            errorSummaryVm = errorSummaryDiv.length > 0 ?
                ko.dataFor(errorSummaryDiv[0]) : null;

        if (!errorSummaryVm) {
            return ("collapse");
        }

        return (errorSummaryVm.errorsPresentCheck());
    }

    setUidTabActive($data, $event) {
        let target = jQuery($event.currentTarget),
            headerText = target.data('header-text'),
            allTabs = target.parent().find('.login-pane-selector');

        allTabs.text(function (pIndex) {
            let tab = jQuery(this);
            return (tab.data('link-text'));
        });

        target.text(headerText);
        this.activeTab("uid");
    }

    setNoUidTabActive($data, $event) {
        let target = jQuery($event.currentTarget),
            headerText = target.data('header-text'),
            allTabs = target.parent().find('.login-pane-selector');

        allTabs.text(function (pIndex) {
            let tab = jQuery(this);
            return (tab.data('link-text'));
        });

        target.text(headerText);
        this.activeTab("nouid");
    }

    addErrors(pErrorArray) {
        if (!Array.isArray(pErrorArray)) {
            this.errors([]);
            this.errors.push(pErrorArray);
            return;
        }

        this.errors(pErrorArray);
    }

    validateForm($form) {
        event.preventDefault();
        let target = jQuery($form);

        let allInvalidInputs = target
            .find(".input-validation-error");
        if (allInvalidInputs.length == 0) {
            this.processForm($form);
            return (true);
        }

        allInvalidInputs.each(function (
            pIndex,
            pVal) {
            let item = jQuery(pVal),
                parentFormGroup = item.closest(".form-group");

            if (item.hasClass("empty")) {
                let requiredTooltip = parentFormGroup
                    .find('.require > svg');

                requiredTooltip
                    .tooltip({
                        container: this.parentElement
                    })
                    .tooltip("show");
                setTimeout(
                    (pTooltip) => { pTooltip.tooltip('hide'); },
                    3000,
                    requiredTooltip);
            }
            else if (item.hasClass("not-empty")) {
                let invalidTooltip = parentFormGroup
                    .find('.invalid > svg');

                invalidTooltip
                    .tooltip({
                        container: this.parentElement
                    })
                    .tooltip("show");
                setTimeout(
                    (pTooltip) => { pTooltip.tooltip('hide'); },
                    3000,
                    invalidTooltip);
            }
        });
    }

    processForm($form) {
        const koContext = this;
        jQuery.ajax({
            type: "POST",
            url: $form.action,
            context: $form,
            dataType: "json",
            data: jQuery($form)
                .serialize(),
            success: function (
                pData,
                pStatus,
                pXhr) {
                if (pData.success) {
                    document.location.href = pData.redirectUrl;
                } else {
                    this.addErrors(pData.errors);
                }
            }.bind(koContext),
            error: function (
                pXhr,
                pStatus,
                pError) {
                this.addErrors(pError.toString());
            }.bind(koContext)
        })
    }
}