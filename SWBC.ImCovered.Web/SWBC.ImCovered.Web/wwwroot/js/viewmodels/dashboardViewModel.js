class DashboardViewModel {    
    constructor(pDataObj) {        
        this.uploadonly = ko.observable(
            pDataObj.uploadOnly || true);
        this.insuranceStatus = ko.observable(
            pDataObj.insuranceStatus || null);
        this.friendlyStatus = ko.observable(
            pDataObj.friendlyStatus || null);
        this.insuranceStatusClass = ko.observable(
            pDataObj.insuranceStatusClass || null);
        this.firstName = ko.observable(
            pDataObj.firstName || null);

        this.displayUploadOnly = ko.pureComputed(function () {
            return (this.uploadonly ? "d-none" : "");
        }, this);
    }
}