class LoginPageInputBoxViewModel {    
    constructor(pDataObj) {

        pDataObj = pDataObj || {};

        this.requiredErrorMessage = ko.observable(
            pDataObj.requiredErrorMessage || null);
        this.invalidErrorMessage = ko.observable(
            pDataObj.invalidErrorMessage || null);
        this.inputLabel = ko.observable(
            pDataObj.inputLabel || null);
        this.inputValue = ko.observable(
            pDataObj.inputValue || null);
        this.inputId = ko.observable(
            pDataObj.inputId || null);
        this.inputAttr = ko.observable(
            this.getInputAttr(
                pDataObj.inputAttr,
                pDataObj.inputId));
        this.inputFormat = pDataObj.inputFormat || null;

        this.isEmpty = ko.pureComputed(
            this.isEmptyCheck, this);
        this.isInvalid = ko.pureComputed(
            this.isInvalidCheck, this);
        this.isInputValid = ko.pureComputed(
            this.isInputValidCheck, this);
    }

    isInvalidCheck() {
        if (!this.inputFormat) {
            return ("d-none");
        }
        let inputVal = ko
            .utils
            .unwrapObservable(
            this.inputValue);

        if (!inputVal || !inputVal.trim()) {
            return ("d-none");
        }

        let tester = RegExp(this.inputFormat);
        return (!tester.test(inputVal) ? "" : "d-none");
    }

    isEmptyCheck() {
        let inputVal = ko
            .utils
            .unwrapObservable(
                this.inputValue);
        return (!inputVal || !inputVal.trim() ? "" : "d-none");
    }

    isInputValidCheck() {
        let inputVal = ko
            .utils
            .unwrapObservable(
            this.inputValue);

        let isEmpty = !inputVal ||
            !inputVal.trim();
        let isInvalid = this.inputFormat &&
            !RegExp(this.inputFormat).test(inputVal);

        if (isEmpty || isInvalid) {
            if (isEmpty) {
                return ("input-validation-error empty");
            }
            return ("input-validation-error not-empty");
        }

        return ("valid");
    }

    getInputAttr(pInputAttr, pInputId) {
        pInputAttr = pInputAttr || {};
        pInputId = pInputId || null;

        pInputAttr.id = pInputId;
        pInputAttr.name = pInputId;
        return (pInputAttr);
    }

    static getTemplate(pBaseUrl) {
        pBaseUrl = (!pBaseUrl || !pBaseUrl.trim() ?
            '/' : pBaseUrl);
        let templatePath = "js/components/loginPageInputBoxComponent.html";

        return (`${pBaseUrl}${templatePath}`);
    }
}