class ErrorSummaryViewModel {    
    constructor(pDataObj) {

        pDataObj = pDataObj || {};

        this.errorArray = ko.observableArray(
            pDataObj.errorArray || []);
    }

    addErrors(pErrorArray) {
        this.errorArray(pErrorArray);
    }

    errorsPresentCheck() {
        return (this.errorArray().length > 0 ?
            "collapse.show" : "collapse");
    }

    static getTemplate(pBaseUrl) {
        pBaseUrl = (!pBaseUrl || !pBaseUrl.trim() ?
            '/' : pBaseUrl);
        let templatePath = "js/components/errorSummary.html";

        return (`${pBaseUrl}${templatePath}`);
    }
}