/// <binding BeforeBuild='clean, copy:libs, min' Clean='clean' ProjectOpened='watch' />
"use strict";

const gulp = require("gulp"),
    rimraf = require("rimraf"),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    terser = require("gulp-terser"),
    sass = require("gulp-sass"),
    sourcemaps = require("gulp-sourcemaps"),
    babel = require("gulp-babel");
    

const basePath = "./wwwroot/";
const npmBasePath = "./node_modules/";
const webrootLibs = basePath + "lib/";

const paths = {
    js: basePath + "js/**/*.js",
    processedJs: basePath + "js/**/*.processed.js",
    minJs: basePath + "js/**/*.min.js",
    css: basePath + "css/**/*.css",
    minCss: basePath + "css/**/*.min.css",
    sass: basePath + "css/scss/*.scss",
    sassDest: basePath + "css",
    concatJsMinDest: basePath + "js/site.min.js",
    concatJsProcessedDest: basePath + "js/site.processed.js",
    concatCssDest: basePath + "css/site.min.css",
    bootstrapCopy: npmBasePath + "bootstrap/dist/**",
    bootstrapDest: webrootLibs + "bootstrap/",
    jqueryCopy: npmBasePath + "jquery/dist/",
    jqueryDest: webrootLibs + "jquery/",
    jqueryValCopy: npmBasePath + "jquery-validation/dist/**/*.js",
    jqueryValDest: webrootLibs + "jquery-validation/",
    jqueryValUnobtrusiveCopy: npmBasePath + "jquery-validation-unobtrusive/dist/*.js",
    jqueryValUnobtrusiveDest: webrootLibs + "jquery-validation-unobtrusive/",
    fontAwesomeCopy: npmBasePath + "@fortawesome/fontawesome-free/js/*.js",
    fontAwesomeDest: webrootLibs + "fontawesome/",
    knockoutCopy: npmBasePath + "knockout/build/output/*.js",
    knockoutDest: webrootLibs + "knockoutjs/"
};

gulp.task("clean:js", done => rimraf(paths.concatJsMinDest, done));
gulp.task("clean:css", done => rimraf(paths.concatCssDest, done));
gulp.task("clean:libs", done => rimraf(webrootLibs + "*", done));
gulp.task("clean", gulp.series(["clean:js", "clean:css", "clean:libs"]));

gulp.task("preprocess:js", () => {
    return gulp.src([paths.js, "!" + paths.minJs, "!" + paths.processedJs], { base: basePath + "js" })
        .pipe(concat(paths.concatJsProcessedDest))
        .pipe(babel({
            presets: ["env"]
        }))
        .pipe(gulp.dest("."));
});

gulp.task("js:watch", () => {
    return gulp.watch(
        [paths.js, "!" + paths.minJs, "!" + paths.processedJs],
        gulp.series(["preprocess:js", "min:js"]));
});

gulp.task("min:js", () => {
    return gulp.src([paths.processedJs, "!" + paths.minJs], { base: "." })
        .pipe(sourcemaps.init())
        .pipe(concat(paths.concatJsMinDest))
        .pipe(terser())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest("."));
});

gulp.task("sass:css", () => {
    return gulp.src(paths.sass)
        .pipe(sourcemaps.init())
        .pipe(sass().on("error", sass.logError))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest(paths.sassDest));
});

gulp.task("sass:watch", () => {
    return gulp.watch(
        paths.sass,
        gulp.series(["sass:css", "min:css"]));
});

gulp.task("watch", gulp.parallel("sass:watch", "js:watch"));

gulp.task("min:css", () => {
    return gulp.src([paths.css, "!" + paths.minCss])
        .pipe(concat(paths.concatCssDest))
        .pipe(cssmin())
        .pipe(gulp.dest("."));
});

gulp.task("min", gulp.series(["preprocess:js", "min:js", "sass:css", "min:css"]));

gulp.task("copy:bootstrap", () => {
    return (gulp.src(paths.bootstrapCopy)
        .pipe(gulp.dest(paths.bootstrapDest)));
});

gulp.task("copy:jquery", () => {
    return (gulp.src([paths.jqueryCopy + "**", "!" + paths.jqueryCopy + "*.slim*"])
        .pipe(gulp.dest(paths.jqueryDest)));
});

gulp.task("copy:jqueryValidation", () => {
    return (gulp.src(paths.jqueryValCopy)
        .pipe(gulp.dest(paths.jqueryValDest)));
});

gulp.task("copy:jqueryValidationUnobtrusive", () => {
    return (gulp.src(paths.jqueryValUnobtrusiveCopy)
        .pipe(gulp.dest(paths.jqueryValUnobtrusiveDest)));
});

gulp.task("copy:fontawesome", () => {
    return (gulp.src(paths.fontAwesomeCopy)
        .pipe(gulp.dest(paths.fontAwesomeDest)));
});

gulp.task("copy:knockoutjs", () => {
    return (gulp.src(paths.knockoutCopy)
        .pipe(gulp.dest(paths.knockoutDest)));
});

gulp.task("copy:libs", gulp.series([
    "copy:bootstrap",
    "copy:jquery",
    "copy:fontawesome",
    "copy:knockoutjs",
    "copy:jqueryValidation",
    "copy:jqueryValidationUnobtrusive"]));

// A 'default' task is required by Gulp v4
gulp.task("default", gulp.series(["min"]));