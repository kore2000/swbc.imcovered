﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.EntityFrameworkCore;

using SWBC.ImCovered.DAL;
using SWBC.ImCovered.Services.Classes;
using SWBC.ImCovered.Services.Interfaces;
using SWBC.ImCovered.Routes.Class;
using SWBC.ImCovered.StateManagement.Classes;
using Custom = SWBC.ImCovered.StateManagement.Interfaces;

namespace SWBC.ImCovered.Web
{
    public class Startup
    {
        #region <Private members>

        private readonly IConfiguration _configuration;

        #endregion

        #region <Public accessors>

        public IConfiguration Configuration => (_configuration);

        #endregion

        #region <Constructor>

        public Startup(
            IConfiguration pConfiguration)
        {
            _configuration = pConfiguration ??
                throw new ArgumentNullException(
                    nameof(pConfiguration));
        }

        #endregion

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(
            IServiceCollection pServices)
        {
            pServices.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
                options.HttpOnly = HttpOnlyPolicy.Always;
            });

            pServices.AddSession(options =>
            {
                options.Cookie.HttpOnly = true;
                options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                options.IdleTimeout = TimeSpan.FromMinutes(10);
                options.Cookie.IsEssential = true;
            });

            pServices
                .AddMvc()
                .AddViewOptions(options =>
                {
                    options.HtmlHelperOptions.ClientValidationEnabled = false;
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            pServices.AddHttpContextAccessor();

            AddImCoveredDbContext(pServices);
            AddCpi01DbContext(pServices);
            AddCpi99DbContext(pServices);
            AddMortgage01DbContext(pServices);
            AddMortgage21DbContext(pServices);
            AddMortgage22DbContext(pServices);
            AddMortgage99DbContext(pServices);

            pServices.AddScoped<Custom.ISession, Session>();
            pServices.AddScoped<ISiteServices, SiteServices>();
            pServices.AddScoped<IBrandServices, BrandServices>();
            pServices.AddScoped<IPolicyServices, PolicyServices>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder pApp,
            IHostingEnvironment pEnv)
        {
            if (pEnv.IsDevelopment())
            {
                pApp.UseDeveloperExceptionPage();
            }
            else
            {
                pApp.UseExceptionHandler("/Home/Error");
                pApp.UseHsts();
            }

            pApp.UseHttpsRedirection();
            pApp.UseStaticFiles();
            pApp.UseCookiePolicy();
            pApp.UseSession();

            pApp.UseMvc(routes =>
            {
                routes.Routes.Add(new BrandedRoute(routes.DefaultHandler));
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Policy}/{action=Index}/");
            });
        }

        #region <Private methods>

        private void AddImCoveredDbContext(
            IServiceCollection pServices)
        {
            var imcContextConnectString = _configuration
                .GetSection("connectionStrings:site:ImCoveredContext")
                .Value;
            if (!string.IsNullOrWhiteSpace(imcContextConnectString))
            {
                pServices
                    .AddDbContext<ImCoveredContext>(pOptions =>
                        pOptions.UseSqlServer(imcContextConnectString));
            }
        }

        private void AddCpi01DbContext(
            IServiceCollection pServices)
        {
            var cpi01ContextConnectString = _configuration
                .GetSection("connectionStrings:cpi:Lib01NCPWebFilesContext")
                .Value;
            if (!string.IsNullOrWhiteSpace(cpi01ContextConnectString))
            {
                pServices
                    .AddDbContext<Cpi01DbContext>(pOptions =>
                        pOptions.UseSqlServer(cpi01ContextConnectString));                        
            }
        }

        private void AddCpi99DbContext(
            IServiceCollection pServices)
        {
            var cpi99ContextConnectString = _configuration
                .GetSection("connectionStrings:cpi:Lib99NCPWebFilesContext")
                .Value;
            if (!string.IsNullOrWhiteSpace(cpi99ContextConnectString))
            {
                pServices
                    .AddDbContext<Cpi99DbContext>(pOptions =>
                        pOptions.UseSqlServer(cpi99ContextConnectString));
            }
        }

        private void AddMortgage01DbContext(
            IServiceCollection pServices)
        {
            var mtg01ContextConnectString = _configuration
                .GetSection("connectionStrings:mortgage:Lib01MTGWebFilesContext")
                .Value;
            if (!string.IsNullOrWhiteSpace(mtg01ContextConnectString))
            {
                pServices
                    .AddDbContext<Mortgage01DbContext>(pOptions =>
                        pOptions.UseSqlServer(mtg01ContextConnectString));
            }
        }

        private void AddMortgage21DbContext(
            IServiceCollection pServices)
        {
            var mtg21ContextConnectString = _configuration
                .GetSection("connectionStrings:mortgage:Lib21MTGWebFilesContext")
                .Value;
            if (!string.IsNullOrWhiteSpace(mtg21ContextConnectString))
            {
                pServices
                    .AddDbContext<Mortgage21DbContext>(pOptions =>
                        pOptions.UseSqlServer(mtg21ContextConnectString));
            }
        }

        private void AddMortgage22DbContext(
            IServiceCollection pServices)
        {
            var mtg22ContextConnectString = _configuration
                .GetSection("connectionStrings:mortgage:Lib22MTGWebFilesContext")
                .Value;
            if (!string.IsNullOrWhiteSpace(mtg22ContextConnectString))
            {
                pServices
                    .AddDbContext<Mortgage22DbContext>(pOptions =>
                        pOptions.UseSqlServer(mtg22ContextConnectString));
            }
        }

        private void AddMortgage99DbContext(
            IServiceCollection pServices)
        {
            var mtg99ContextConnectString = _configuration
                .GetSection("connectionStrings:mortgage:Lib99MTGWebFilesContext")
                .Value;
            if (!string.IsNullOrWhiteSpace(mtg99ContextConnectString))
            {
                pServices
                    .AddDbContext<Mortgage99DbContext>(pOptions =>
                        pOptions.UseSqlServer(mtg99ContextConnectString));
            }
        }

        #endregion
    }
}
