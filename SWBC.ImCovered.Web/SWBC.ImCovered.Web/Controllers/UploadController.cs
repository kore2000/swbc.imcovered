﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Http;
using System.Linq;

using SWBC.ImCovered.Models;
using SWBC.ImCovered.Services.Interfaces;
using Custom = SWBC.ImCovered.StateManagement.Interfaces;
using SWBC.ImCovered.DAL.Classes.DataTransferObjects;
using SWBC.ImCovered.Enums.Classes;
using SWBC.ImCovered.Resources;

namespace SWBC.ImCovered.Web.Controllers
{
    [Route("[controller]/[action]")]
    public class UploadController :
        BaseController
    {
        #region <Private members>

        private readonly Custom.ISession _session;

        #endregion

        #region <Constructors>

        static UploadController()
        {
        }

        public UploadController(
            Custom.ISession pSession)
        {
            _session = pSession ??
                throw new ArgumentNullException(
                    nameof(pSession));
        }

        #endregion

        #region <Views>

        [HttpGet("{pUidSecure}")]
        public async Task<IActionResult> Dashboard(
            [FromRoute] string pUidSecure)
        {
            return (View("dashboard", pUidSecure));
        }

        [HttpGet]
        public IActionResult MultipleAccounts(
            MultipleAccountDocumentUploadModel pModel)
        {
            return (View("multipleAccounts", pModel));
        }

        #endregion

        #region <DataApi>

        #endregion

        #region <Private methods>

        #endregion
    }
}
