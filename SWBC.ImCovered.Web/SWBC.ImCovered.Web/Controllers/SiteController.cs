﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Net.Mime;

namespace SWBC.ImCovered.Web.Controllers
{
    public class SiteController:
        BaseController
    {
        #region <Private members>

        private static readonly string _imageLogoPath;
        private static readonly string _defaultLogoImage;
        private readonly IHostingEnvironment _hostingEnvironment;

        #endregion

        #region <Constructors>

        static SiteController()
        {
            _imageLogoPath = "\\images\\logos\\";
            _defaultLogoImage = "imcovered_logo.png";
        }

        public SiteController(
            IHostingEnvironment pHostingEnvironment)
        {
            _hostingEnvironment = pHostingEnvironment ??
                throw new ArgumentNullException(
                    nameof(pHostingEnvironment));
        }

        #endregion

        [HttpGet]
        public IActionResult GetHeaderLogoUrl()
        {
            string imageMappedPath = Path
                .Combine(
                    _hostingEnvironment.WebRootPath,
                    _imageLogoPath);

            string logoFilename = _defaultLogoImage;

            logoFilename = Path.Combine(
                imageMappedPath,
                logoFilename);

            return (File(logoFilename, "image/png"));
        }
    }
}
