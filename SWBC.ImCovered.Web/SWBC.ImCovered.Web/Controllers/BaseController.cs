﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Linq;

namespace SWBC.ImCovered.Web.Controllers
{
    public class BaseController:
        Controller
    {
        #region <Protected methods>

        protected IActionResult ReturnInvalidModelState()
        {
            var errorsFound = ModelState
                .Values
                .Where(w => w.ValidationState == ModelValidationState.Invalid)
                .SelectMany(sm => sm.Errors)
                .Select(s => s.ErrorMessage)
                .ToArray();

            return (Json(new
            {
                success = false,
                errors = errorsFound
            }));
        }

        #endregion
    }
}
