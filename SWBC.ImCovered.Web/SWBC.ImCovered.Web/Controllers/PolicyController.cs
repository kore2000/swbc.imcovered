﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Http;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;

using SWBC.ImCovered.Models;
using SWBC.ImCovered.Services.Interfaces;
using Custom = SWBC.ImCovered.StateManagement.Interfaces;
using SWBC.ImCovered.DAL.Classes.DataTransferObjects;
using SWBC.ImCovered.Enums.Classes;
using SWBC.ImCovered.Resources;

namespace SWBC.ImCovered.Web.Controllers
{
    [Route("[controller]/[action]")]
    public class PolicyController :
        BaseController
    {
        #region <Private members>

        private readonly static string _defaultBrandLabel;
        private readonly IBrandServices _brandServices;
        private readonly IPolicyServices _policyServices;
        private readonly Custom.ISession _session;

        #endregion

        #region <Constructors>

        static PolicyController()
        {
            _defaultBrandLabel = "default";
        }

        public PolicyController(
            IBrandServices pBrandServices,
            IPolicyServices pPolicyServices,
            Custom.ISession pSession)
        {
            _brandServices = pBrandServices ??
                throw new ArgumentNullException(
                    nameof(pBrandServices));
            _policyServices = pPolicyServices ??
                throw new ArgumentNullException(
                    nameof(pPolicyServices));
            _session = pSession ??
                throw new ArgumentNullException(
                    nameof(pSession));
        }

        #endregion

        #region <Views>

        [HttpGet("/")]
        [HttpGet("/{pUid:regex(^[[0-9]]{{5,12}}$)}")]
        public async Task<IActionResult> Index(
            [FromRoute] int? pUid)
        {
            var modelToSend = new LoginModel()
            {
                Uid = pUid,
                ActiveTab = "uid",
                Brand = await GetBrandFromSession(),
                BaseUrl = Url.Content("~/")
            };

            return (View("index", modelToSend));
        }

        [HttpGet("/{pBrand:regex(^[[A-Za-z0-9]]{{1,50}}$)}/{pUid:regex(^[[0-9]]{{5,12}}$)?}")]
        public IActionResult Index(
            [FromRoute] string pBrand,
            [FromRoute] int? pUid)
        {
            var currentUrl = UriHelper.GetDisplayUrl(Request);
            var currentUrlUri = new Uri(currentUrl);

            UriHelper.FromAbsolute(
                currentUrlUri.AbsoluteUri,
                out string scheme,
                out HostString currentHostString,
                out PathString currentPath,
                out QueryString currentQueryString,
                out FragmentString currentFragmentString);

            string modifiedHost = currentUrlUri
                .Host
                .Replace("www.", string.Empty)
                .Insert(0, $"{pBrand}.");
            var hostString = (!currentUrlUri.IsDefaultPort) ?
                new HostString(modifiedHost, currentUrlUri.Port) :
                new HostString(modifiedHost);

            if (pUid.HasValue)
            {
                currentQueryString = new QueryString(
                    currentQueryString
                        .Add("pUid", pUid.Value.ToString())
                        .Value);
            }

            var redirectUrl = UriHelper.BuildAbsolute(
                Request.Scheme,
                hostString,
                query: currentQueryString,
                fragment: currentFragmentString);

            return (RedirectPermanent(redirectUrl));
        }

        [HttpGet]
        public IActionResult DashboardRestricted()
        {
            return (View("dashboard"));
        }

        [HttpGet("{pUidSecure}")]
        public IActionResult Dashboard(
            [FromRoute] string pUidSecure)
        {
            return (View("dashboard", pUidSecure));
        }

        [HttpPost]
        public async Task<IActionResult> Dashboard(
            [FromForm] DashboardModel pModel)
        {
            if (!ModelState.IsValid)
            {
                return (ReturnInvalidModelState());
            }

            var policyLookupStatus = await _policyServices.PolicyLookupByUid(
                pModel.Uid,
                pModel.ZipCode);

            if (policyLookupStatus == PolicyLookupEnums.Status.UidNotFound)
            {
                return (Json(new
                {
                    success = false,
                    errors = Policy.UidNotFound
                }));
            }
            else if (policyLookupStatus == PolicyLookupEnums.Status.UidFoundNotAuthenticated)
            {
                return (Json(new
                {
                    success = false,
                    errors = Policy.UidNotAuthenticated
                }));
            }
            else if (policyLookupStatus == PolicyLookupEnums.Status.UidFoundAuthenticated)
            {
                return (Json(new
                {
                    success = true,
                    redirectUrl = $"{Url.Action("Dashboard","Policy")}/{pModel.Uid}"
                }));
            }

            return (Json(new
            {
                success = false,
                errors = Policy.UnknownError
            }));
        }

        [HttpPost]
        public async Task<IActionResult> Upload(
            [FromForm] DocumentUploadModel pModel)
        {
            if (!ModelState.IsValid)
            {
                return (ReturnInvalidModelState());
            }
            else if (!Enum.IsDefined(
                typeof(DbContextEnums.InsuranceType),
                pModel.LoanType) ||
                pModel.LoanType == DbContextEnums.InsuranceType.None)
            {
                return (Json(new
                {
                    success = false,
                    errors = "Please select a loan type"
                }));
            }

            var searchStatus = await _policyServices.PolicyLookupByName(
                pModel.LastName,
                pModel.ZipCode,
                pModel.LoanType);

            if (searchStatus == PolicyLookupEnums.Status.NoAccountsFound)
            {
                return (Json(new
                {
                    success = false,
                    errors = Policy.AccountNotFound
                }));
            }
            else if (searchStatus == PolicyLookupEnums.Status.SingleAccountFound)
            {
                var securedUid = _policyServices.GetSecureIdFromSession();
                return (Json(new
                {
                    success = true,
                    redirectUrl = $"{Url.Action("Dashboard", "Upload")}/{securedUid}"
                }));
            }
            else if (searchStatus == PolicyLookupEnums.Status.MultipleAccountsFound)
            {
                return (Json(new
                {
                    success = true,
                    redirectUrl = Url.Action(
                        "multipleAccounts",
                        "Upload",
                        new
                        {
                            pModel.LastName,
                            pModel.ZipCode,
                            pModel.LoanType
                        })
                }));
            }

            return (Json(new
            {
                success = false,
                errors = Policy.UnknownError
            }));
        }

        #endregion

        #region <DataApi>

        [Produces("application/json")]
        [HttpGet("{pUid:regex(^[[0-9]]{{5,12}}$)?}")]
        public async Task<IActionResult> GetCustomerInfo(
            int? pUid)
        {
            if (!pUid.HasValue)
            {
                return (Json(new
                {
                    uploadOnly = true,
                    friendlyStatus = Resources.Dashboard.AccessRestricted,
                    firstName = Resources.Dashboard.AccessRestrictedFirstName,
                    insuranceStatus = string.Empty,
                    insuranceStatusClass = "neutral"
                }));
            }

            var customerAccount = await _policyServices.GetPolicyInformation(
                pUid.ToString());

            //go get data
            return (Json(new
            {
                uploadOnly = false,
                friendlyStatus = Resources.Dashboard.DemoFriendlyStatus,
                firstName = customerAccount.Name,
                insuranceStatus = Resources.Dashboard.DemoStatus,
                insuranceStatusClass = "bad"
            }));
        }

        #endregion

        #region <Private methods>

        private async Task<Brand> GetBrandFromSession()
        {
            if (!ControllerContext
                    .RouteData
                    .Values
                    .TryGetValue("brand", out object injectedBrandName))
            {
                injectedBrandName = _defaultBrandLabel;
            }

            var brandShortName = injectedBrandName as string ??
                _defaultBrandLabel;

            if (_session.SiteBrand != null &&
                _session.SiteBrand.UrlShortName.Name.Equals(
                    brandShortName,
                    StringComparison.CurrentCultureIgnoreCase))
            {
                return (_session.SiteBrand);
            }

            var retrievedBrand = _brandServices
                .GetBrandAsync(brandShortName);

            if (retrievedBrand.IsCompleted)
            {
                _session.SiteBrand = retrievedBrand.Result;
                return (_session.SiteBrand);
            }
            _session.SiteBrand = await retrievedBrand;
            return (_session.SiteBrand);
        }

        #endregion
    }
}
