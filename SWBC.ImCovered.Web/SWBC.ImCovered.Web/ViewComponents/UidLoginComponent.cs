﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

using SWBC.ImCovered.Models;

namespace SWBC.ImCovered.Web.ViewComponents
{
    public class UidLoginComponent :
        ViewComponent
    {
        #region <Private members>

        #endregion

        #region <Constructors>

        public UidLoginComponent()
        {
        }

        #endregion

        public async Task<IViewComponentResult> InvokeAsync(
            UidLoginComponentModel pModel)
        {
            var viewPath = "~/Views/Shared/Components/_uidLogin.cshtml";

            return (await Task.FromResult(
                View(viewPath, pModel)));
        }
    }
}
