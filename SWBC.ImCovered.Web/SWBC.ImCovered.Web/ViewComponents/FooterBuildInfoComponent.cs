﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

using SWBC.ImCovered.Services.Interfaces;
using SWBC.ImCovered.Models;

namespace SWBC.ImCovered.Web.ViewComponents
{
    public class FooterBuildInfoComponent:
        ViewComponent
    {
        #region <Private members>

        private readonly ISiteServices _siteServices;

        #endregion

        #region <Constructors>

        public FooterBuildInfoComponent(
            ISiteServices pSiteServices)
        {
            _siteServices = pSiteServices ??
                throw new ArgumentNullException(
                    nameof(pSiteServices));
        }

        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var modelToSend = new FooterBuildInfoComponentModel()
            {
                Year = _siteServices.GetYear(),
                ReleaseTag = _siteServices.GetReleaseTag(),
                ReleaseBuild = _siteServices.GetReleaseBuild(),
                DisplayBuildInfo = _siteServices.GetDisplayBuildInfo()
            };

            var viewPath = "~/Views/Shared/Components/_footerBuildInfo.cshtml";

            return (await Task.FromResult(
                View(viewPath, modelToSend)));
        }
    }
}
