﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

using SWBC.ImCovered.Models;

namespace SWBC.ImCovered.Web.ViewComponents
{
    public class DocumentUploadComponent :
        ViewComponent
    {
        #region <Private members>

        #endregion

        #region <Constructors>

        public DocumentUploadComponent()
        {
        }

        #endregion

        public async Task<IViewComponentResult> InvokeAsync(
            DocumentUploadComponentModel pModel)
        {
            var viewPath = "~/Views/Shared/Components/_documentUpload.cshtml";

            return (await Task.FromResult(
                View(viewPath, pModel)));
        }
    }
}
