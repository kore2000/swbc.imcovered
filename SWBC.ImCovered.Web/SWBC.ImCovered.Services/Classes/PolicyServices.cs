﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Generic;
using System.Linq;

using SWBC.ImCovered.Services.Interfaces;
using SWBC.ImCovered.DAL;
using SWBC.ImCovered.Enums.Classes;
using SWBC.ImCovered.DAL.Classes.DataTransferObjects;
using Custom = SWBC.ImCovered.StateManagement.Interfaces;
using System.Data.SqlClient;

namespace SWBC.ImCovered.Services.Classes
{
    public class PolicyServices:
        BaseServices,
        IPolicyServices
    {
        #region <Private members>

        private readonly static List<DbContextEnums.AgencyNumber> _agencyList;
        private readonly IDictionary<DbContextEnums.AgencyNumber, BaseCpiDbContext> _cpiDbContexts;
        private readonly IDictionary<DbContextEnums.AgencyNumber, BaseMortgageDbContext> _mtgDbContexts;
        private readonly Custom.ISession _session;
        private double _uniqueId;
        private DbContextEnums.InsuranceType _insuranceType;
        private DbContextEnums.AgencyNumber _agency;

        #endregion

        #region <Public accessors>

        #endregion

        #region <Constructors>

        static PolicyServices()
        {
            _agencyList = new List<DbContextEnums.AgencyNumber>(5)
            {
                DbContextEnums.AgencyNumber.AgencyOne,
                DbContextEnums.AgencyNumber.AgencyTwentyOne,
                DbContextEnums.AgencyNumber.AgencyTwentyTwo,
                DbContextEnums.AgencyNumber.AgencyNinetyNine
            };
        }

        public PolicyServices(
            Cpi01DbContext pCpi01DbContext,
            Cpi99DbContext pCpi99DbContext,
            Mortgage01DbContext pMtg01DbContext,
            Mortgage21DbContext pMtg21DbContext,
            Mortgage22DbContext pMtg22DbContext,
            Mortgage99DbContext pMtg99DbContext,
            Custom.ISession pSession) :
            base()
        {
            _session = pSession ??
                throw new ArgumentNullException(
                    nameof(pSession));
            var cpi01DbContext = pCpi01DbContext ??
                throw new ArgumentNullException(
                    nameof(pCpi01DbContext));
            var cpi99DbContext = pCpi99DbContext ??
                throw new ArgumentNullException(
                    nameof(pCpi99DbContext));
            var mtg01DbContext = pMtg01DbContext ??
                throw new ArgumentNullException(
                    nameof(pMtg01DbContext));
            var mtg21DbContext = pMtg21DbContext ??
                throw new ArgumentNullException(
                    nameof(pMtg21DbContext));
            var mtg22DbContext = pMtg22DbContext ??
                throw new ArgumentNullException(
                    nameof(pMtg22DbContext));
            var mtg99DbContext = pMtg99DbContext ??
                throw new ArgumentNullException(
                    nameof(pMtg99DbContext));

            _cpiDbContexts = new Dictionary<
                DbContextEnums.AgencyNumber,
                BaseCpiDbContext>(5)
            {
                { DbContextEnums.AgencyNumber.AgencyOne, cpi01DbContext },
                { DbContextEnums.AgencyNumber.AgencyNinetyNine, cpi99DbContext }
            };
            _mtgDbContexts = new Dictionary<
                DbContextEnums.AgencyNumber,
                BaseMortgageDbContext>(5)
            {
                { DbContextEnums.AgencyNumber.AgencyOne, mtg01DbContext },
                { DbContextEnums.AgencyNumber.AgencyTwentyOne, mtg21DbContext },
                { DbContextEnums.AgencyNumber.AgencyTwentyTwo, mtg22DbContext },
                { DbContextEnums.AgencyNumber.AgencyNinetyNine, mtg99DbContext }
            };
        }

        #endregion

        public async Task<PolicyLookupEnums.Status> PolicyLookupByUid(
            string pUid,
            int pZip)
        {
            ThrowExceptionIfEmpty(
                pUid,
                nameof(pUid));

            try
            {
                ParseUid(pUid);
            }
            catch (Exception pEx)
            {
                //log exception
                return (PolicyLookupEnums.Status.UidNotFound);
            }

            using (var cancelTokenSource = new CancellationTokenSource())
            {
                var baseDbContext = GetProperContext();
                if (baseDbContext == null)
                {
                    return (PolicyLookupEnums.Status.UidNotFound);
                }

                if (_insuranceType == DbContextEnums.InsuranceType.Cpi)
                {
                    return (await RetrieveAndAuthenticateCpiAccount(
                        pZip,
                        cancelTokenSource,
                        baseDbContext));
                }
                else if (_insuranceType == DbContextEnums.InsuranceType.Mortgage)
                {
                    return (PolicyLookupEnums.Status.None);
                }
            }

            return (PolicyLookupEnums.Status.None);
        }

        public async Task<PolicyLookupEnums.Status> PolicyLookupByName(
            string pName,
            int pZip,
            DbContextEnums.InsuranceType pInsuranceType)
        {
            ThrowExceptionIfEmpty(
                pName,
                nameof(pName));

            var foundAccounts = new Dictionary<
                DbContextEnums.AgencyNumber,
                IEnumerable<string>>(200);

            if (pInsuranceType == DbContextEnums.InsuranceType.Cpi)
            {
                await LookupCpiAccounts(
                    pName,
                    pZip,
                    foundAccounts);
            }
            else if (pInsuranceType == DbContextEnums.InsuranceType.Mortgage)
            {
                await LookupMortgageAccounts(
                    pName,
                    pZip,
                    foundAccounts);
            }

            int numberOfFoundAccounts = foundAccounts
                .Sum(s => s.Value.Count());

            if (numberOfFoundAccounts <= 0)
            {
                return (PolicyLookupEnums.Status.NoAccountsFound);
            }
            else if (numberOfFoundAccounts == 1)
            {
                var singleAccount = foundAccounts
                    .FirstOrDefault(fod => fod.Value.Any());
                var dbContext = GetProperContext(
                    pInsuranceType,
                    singleAccount.Key);
                var primaryKey = singleAccount
                    .Value?
                    .FirstOrDefault();

                if (pInsuranceType == DbContextEnums.InsuranceType.Cpi)
                {
                    _session.RetrievedAccount = await RetrieveCpiAccountByKey(
                        primaryKey,
                        dbContext);
                }
                else if (pInsuranceType == DbContextEnums.InsuranceType.Mortgage)
                {
                    _session.RetrievedAccount = await RetrieveMortgageAccountByKey(
                        primaryKey,
                        dbContext);
                }
                return (PolicyLookupEnums.Status.SingleAccountFound);
            }
            else if (numberOfFoundAccounts > 1)
            {
                return (PolicyLookupEnums.Status.MultipleAccountsFound);
            }

            return (PolicyLookupEnums.Status.NoAccountsFound);
        }

        public async Task<CpiAccountInformationFI100> GetPolicyInformation(
            string pUid)
        {
            ThrowExceptionIfEmpty(
                pUid,
                nameof(pUid));

            ParseUid(pUid);

            using (var cancelTokenSource = new CancellationTokenSource())
            {
                var baseDbContext = GetProperContext();
                if (baseDbContext == null)
                {
                    throw new InvalidOperationException(
                        "Unable to retrieve DB context");
                }

                if (_insuranceType == DbContextEnums.InsuranceType.Cpi)
                {
                    return (await RetrieveCpiAccount(
                        cancelTokenSource,
                        baseDbContext));
                }
            }

            return (null);
        }

        public double GetSecureIdFromSession()
        {
            if (_session.RetrievedAccount is CpiAccountInformationFI100)
            {
                var convertedAccount = _session.RetrievedAccount as CpiAccountInformationFI100;
                if (convertedAccount != null &&
                    convertedAccount.FI104 != null &&
                    convertedAccount.FI104.ReferenceNumber.HasValue)
                {
                    return (convertedAccount.FI104.ReferenceNumber.Value);
                }
            }
            else if (_session.RetrievedAccount is MortgageAccountInformationFMI001)
            {
                var convertedAccount = _session.RetrievedAccount as MortgageAccountInformationFMI001;
                if (convertedAccount != null &&
                    convertedAccount.FMI104 != null &&
                    convertedAccount.FMI104.FirstOrDefault() != null)
                {
                    return (convertedAccount
                        .FMI104
                        .FirstOrDefault()
                        .ReferenceNumber);
                }
            }

            return (double.MinValue);
        }

        #region <Private methods>

        private void ParseUid(
            string pUid)
        {
            if (pUid.Length < 4)
            {
                throw new ArgumentException(
                    $"{nameof(pUid)} is too short",
                    nameof(pUid));
            }

            if (!double.TryParse(pUid, out _uniqueId))
            {
                throw new ArgumentException(
                    $"{nameof(pUid)} does not contain a valid reference number",
                    nameof(pUid));
            }

            var insuranceType = pUid.Substring(pUid.Length - 3, 1);
            if (!Enum.TryParse(
                    insuranceType,
                    true,
                    out _insuranceType) ||
                !Enum.IsDefined(
                    typeof(DbContextEnums.InsuranceType),
                    _insuranceType))
            {
                throw new ArgumentException(
                    $"{nameof(pUid)} does not contain a valid insurance type",
                    nameof(pUid));
            }

            var agency = pUid.Substring(pUid.Length - 2, 2);
            if (!Enum.TryParse(
                agency,
                true,
                out _agency))
            {
                throw new ArgumentException(
                    $"{nameof(pUid)} does not contain a valid agency",
                    nameof(pUid));
            }
        }

        private DbContext GetProperContext()
        {
            return (GetProperContext(_insuranceType, _agency));
                
        }

        private DbContext GetProperContext(
            DbContextEnums.InsuranceType pInsuranceType,
            DbContextEnums.AgencyNumber pAgency)
        {
            if (pInsuranceType == DbContextEnums.InsuranceType.Cpi &&
                _cpiDbContexts.ContainsKey(pAgency))
            {
                return (_cpiDbContexts[pAgency]);
            }
            else if (pInsuranceType == DbContextEnums.InsuranceType.Mortgage &&
                _mtgDbContexts.ContainsKey(pAgency))
            {
                return (_mtgDbContexts[pAgency]);
            }

            return (null);
        }

        private async Task<PolicyLookupEnums.Status> RetrieveAndAuthenticateCpiAccount(
            int pZip,
            CancellationTokenSource pCancelTokenSource,
            DbContext pBaseDbContext)
        {
            ClearSessionRetrievedAccount();

            var cpiDbContext = pBaseDbContext as BaseCpiDbContext;
            try
            {
                var pulledAccount = await cpiDbContext
                    .CpiFI104
                    .FirstOrDefaultAsync(fod =>
                        fod.ReferenceNumber.HasValue &&
                        fod.ReferenceNumber.Value == _uniqueId,
                        pCancelTokenSource.Token);

                if (pulledAccount == null)
                {
                    return (PolicyLookupEnums.Status.UidNotFound);
                }

                var expandedAccountInfo = await cpiDbContext
                    .CpiFI100
                    .FirstOrDefaultAsync(fod =>
                        fod.AccountNumber == pulledAccount.AccountNumber &&
                        fod.LoanNumber.Equals(pulledAccount.LoanNumber) &&
                        fod.LoanSuffix == pulledAccount.LoanSuffix);

                if (expandedAccountInfo == null)
                {
                    //this shouldn't happen, we need to log this
                    return (PolicyLookupEnums.Status.UidNotFound);
                }

                if (expandedAccountInfo.AddressZip == pZip)
                {
                    expandedAccountInfo.FI104 = pulledAccount;
                    _session.RetrievedAccount = expandedAccountInfo;
                    return (PolicyLookupEnums.Status.UidFoundAuthenticated);
                }
                else
                {
                    return (PolicyLookupEnums.Status.UidFoundNotAuthenticated);
                }
            }
            catch (TaskCanceledException pTaskCancelEx)
            {
                //log exception
                throw new InvalidOperationException(
                    $"Unable to locate account ({_uniqueId})",
                    pTaskCancelEx);
            }
        }

        private async Task LookupCpiAccounts(
            string pName,
            int pZip,
            Dictionary<DbContextEnums.AgencyNumber, IEnumerable<string>> pFoundAccounts)
        {
            foreach (var agency in _agencyList)
            {
                var baseDbContext = GetProperContext(
                    DbContextEnums.InsuranceType.Cpi,
                    agency);
                if (baseDbContext == null)
                {
                    continue;
                }

                using (var cancelTokenSource = new CancellationTokenSource())
                {
                    var retrievedAccounts = SearchForCpiAccount(
                            pName,
                            pZip,
                            cancelTokenSource,
                            baseDbContext);
                    if (retrievedAccounts.IsCompleted)
                    {
                        pFoundAccounts.Add(
                            agency,
                            retrievedAccounts.Result);
                        continue;
                    }

                    pFoundAccounts.Add(
                        agency,
                        await retrievedAccounts);
                }
            }
        }
        
        private async Task<IEnumerable<string>> SearchForCpiAccount(
            string pName,
            int pZip,
            CancellationTokenSource pCancelTokenSource,
            DbContext pBaseDbContext)
        {
            var cpiDbContext = pBaseDbContext as BaseCpiDbContext;
            try
            {
                var foundAccounts = await cpiDbContext
                    .CpiFI100
                    .Where(w =>
                        !string.IsNullOrWhiteSpace(w.Name) &&
                        w.Name.Contains(pName, StringComparison.CurrentCultureIgnoreCase) &&
                        w.AddressZip == pZip)
                    .Select(s => s.JoinedPrimaryKey)
                    .ToListAsync(pCancelTokenSource.Token);

                return (foundAccounts);
            }
            catch (TaskCanceledException pTaskCancelEx)
            {
                //log exception
                throw new InvalidOperationException(
                    $"Attempts to locate an account have timed-out ({pName} - CPI)",
                    pTaskCancelEx);
            }
        }

        private async Task<CpiAccountInformationFI100> RetrieveCpiAccount(
            CancellationTokenSource pCancelTokenSource,
            DbContext pBaseDbContext)
        {
            if (_session.RetrievedAccount is CpiAccountInformationFI100)
            {
                var convertedAccount = _session.RetrievedAccount as CpiAccountInformationFI100;
                if (convertedAccount != null &&
                    convertedAccount.FI104.ReferenceNumber == _uniqueId)
                {
                    return (convertedAccount);
                }
            }

            var returnResults = RetrieveCpiAccountByUid(
                _uniqueId,
                pCancelTokenSource,
                pBaseDbContext as BaseCpiDbContext);

            if (returnResults.IsCompleted)
            {
                return (returnResults.Result);
            }

            return (await returnResults);
        }

        private async Task<CpiAccountInformationFI100> RetrieveCpiAccountByKey(
            string pJoinedPrimaryKey,
            DbContext pBaseDbContext)
        {
            if (string.IsNullOrWhiteSpace(pJoinedPrimaryKey))
            {
                //log this
                throw new InvalidOperationException("Primary key is empty");
            }

            var primaryKeyArray = pJoinedPrimaryKey
                .Split(
                    "||",
                    StringSplitOptions.RemoveEmptyEntries);
            if (primaryKeyArray.Length < 3)
            {
                throw new InvalidOperationException("Primary key is not complete");
            }

            string loanNumber = primaryKeyArray[1];

            if (!int.TryParse(primaryKeyArray[0], out int accountNumber) ||
                !int.TryParse(primaryKeyArray[2], out int loanSuffix) ||
                string.IsNullOrWhiteSpace(loanNumber))
            {
                throw new InvalidOperationException("Primary key is invalid");
            }

            using (var cancelTokenSource = new CancellationTokenSource())
            {
                var returnResults = RetrieveCpiAccountByKey(
                    accountNumber,
                    loanSuffix,
                    loanNumber,
                    cancelTokenSource,
                    pBaseDbContext as BaseCpiDbContext);

                if (returnResults.IsCompleted)
                {
                    return (returnResults.Result);
                }

                return (await returnResults);
            }
        }

        private async Task<CpiAccountInformationFI100> RetrieveCpiAccountByUid(
            double pUid,
            CancellationTokenSource pCancelTokenSource,
            BaseCpiDbContext pBaseDbContext)
        {
            try
            {
                var pulledAccount = await pBaseDbContext
                    .CpiFI104
                    .FirstOrDefaultAsync(fod =>
                        fod.ReferenceNumber.HasValue &&
                        fod.ReferenceNumber.Value == pUid,
                        pCancelTokenSource.Token);

                if (pulledAccount == null)
                {
                    return (null);
                }

                return (await RetrieveCpiAccountByKey(
                    pulledAccount.AccountNumber,
                    pulledAccount.LoanSuffix,
                    pulledAccount.LoanNumber,
                    pCancelTokenSource,
                    pBaseDbContext));
            }
            catch (TaskCanceledException pTaskCancelEx)
            {
                //log exception
                throw new InvalidOperationException(
                    $"Unable to locate account ({_uniqueId})",
                    pTaskCancelEx);
            }
        }

        private async Task<CpiAccountInformationFI100> RetrieveCpiAccountByKey(
            int pAccountNumber,
            int pLoanSuffix,
            string pLoanNumber,
            CancellationTokenSource pCancelTokenSource,
            BaseCpiDbContext pBaseDbContext)
        {
            try
            {
                var pulledAccount = await pBaseDbContext
                    .CpiFI100
                    .FirstOrDefaultAsync(fod =>
                        fod.AccountNumber == pAccountNumber &&
                        fod.LoanNumber.Equals(pLoanNumber) &&
                        fod.LoanSuffix == pLoanSuffix,
                        pCancelTokenSource.Token);

                if (pulledAccount == null)
                {
                    return (null);
                }

                var expandedAccountInfo = await pBaseDbContext
                    .CpiFI104
                    .FirstOrDefaultAsync(fod =>
                        fod.AccountNumber == pAccountNumber &&
                        fod.LoanNumber.Equals(pLoanNumber) &&
                        fod.LoanSuffix == pLoanSuffix,
                        pCancelTokenSource.Token);
                if (expandedAccountInfo != null)
                {
                    pulledAccount.FI104 = expandedAccountInfo;
                }

                return (pulledAccount);
            }
            catch (TaskCanceledException pTaskCancelEx)
            {
                //log exception
                throw new InvalidOperationException(
                    $"Unable to locate account ({_uniqueId} - CPI)",
                    pTaskCancelEx);
            }
        }
        
        private async Task LookupMortgageAccounts(
            string pName,
            int pZip,
            Dictionary<DbContextEnums.AgencyNumber, IEnumerable<string>> pFoundAccounts)
        {
            foreach (var agency in _agencyList)
            {
                var baseDbContext = GetProperContext(
                    DbContextEnums.InsuranceType.Mortgage,
                    agency);
                if (baseDbContext == null)
                {
                    continue;
                }

                using (var cancelTokenSource = new CancellationTokenSource())
                {
                    var retrievedAccounts = SearchForMortgageAccount(
                            pName,
                            pZip,
                            cancelTokenSource,
                            baseDbContext);
                    if (retrievedAccounts.IsCompleted)
                    {
                        pFoundAccounts.Add(
                            agency,
                            retrievedAccounts.Result);
                        continue;
                    }

                    pFoundAccounts.Add(
                        agency,
                        await retrievedAccounts);
                }
            }
        }

        private async Task<IEnumerable<string>> SearchForMortgageAccount(
            string pName,
            int pZip,
            CancellationTokenSource pCancelTokenSource,
            DbContext pBaseDbContext)
        {
            var mtgDbContext = pBaseDbContext as BaseMortgageDbContext;
            try
            {
                var foundAccounts = await mtgDbContext
                    .MortgageFMI001
                    .Where(w =>
                        !string.IsNullOrWhiteSpace(w.Name) &&
                        w.Name.Contains(pName, StringComparison.CurrentCultureIgnoreCase) &&
                        w.AddressZip == pZip)
                    .Select(s => s.JoinedPrimaryKey)
                    .ToListAsync(pCancelTokenSource.Token);

                return (foundAccounts);
            }
            catch (TaskCanceledException pTaskCancelEx)
            {
                //log exception
                throw new InvalidOperationException(
                    $"Attempts to locate an account have timed-out ({pName} - MTG)",
                    pTaskCancelEx);
            }
        }

        private async Task<MortgageAccountInformationFMI001> RetrieveMortgageAccountByKey(
            string pJoinedPrimaryKey,
            DbContext pBaseDbContext)
        {
            if (string.IsNullOrWhiteSpace(pJoinedPrimaryKey))
            {
                //log this
                throw new InvalidOperationException("Primary key is empty");
            }

            var primaryKeyArray = pJoinedPrimaryKey
                .Split(
                    "||",
                    StringSplitOptions.RemoveEmptyEntries);
            if (primaryKeyArray.Length < 3)
            {
                throw new InvalidOperationException("Primary key is not complete");
            }

            string loanNumber = primaryKeyArray[1];

            if (!int.TryParse(primaryKeyArray[0], out int accountNumber) ||
                !int.TryParse(primaryKeyArray[2], out int loanSuffix) ||
                string.IsNullOrWhiteSpace(loanNumber))
            {
                throw new InvalidOperationException("Primary key is invalid");
            }

            using (var cancelTokenSource = new CancellationTokenSource())
            {
                var returnResults = RetrieveMortgageAccountByKey(
                    accountNumber,
                    loanSuffix,
                    loanNumber,
                    cancelTokenSource,
                    pBaseDbContext as BaseMortgageDbContext);

                if (returnResults.IsCompleted)
                {
                    return (returnResults.Result);
                }

                return (await returnResults);
            }
        }

        private async Task<MortgageAccountInformationFMI001> RetrieveMortgageAccountByKey(
            int pAccountNumber,
            int pLoanSuffix,
            string pLoanNumber,
            CancellationTokenSource pCancelTokenSource,
            BaseMortgageDbContext pBaseDbContext)
        {
            try
            {
                var pulledAccount = await pBaseDbContext
                    .MortgageFMI001
                    .FirstOrDefaultAsync(fod =>
                        fod.AccountNumber == pAccountNumber &&
                        fod.LoanNumber.Equals(pLoanNumber) &&
                        fod.LoanSuffix == pLoanSuffix,
                        pCancelTokenSource.Token);

                if (pulledAccount == null)
                {
                    return (null);
                }

                var expandedAccountInfo = await pBaseDbContext
                    .MortgageFMI104
                    .Where(s =>
                        s.AccountNumber == pAccountNumber &&
                        s.LoanNumber.Equals(pLoanNumber) &&
                        s.LoanSuffix == pLoanSuffix)
                    .ToListAsync(pCancelTokenSource.Token);
                if (expandedAccountInfo != null)
                {
                    pulledAccount.FMI104 = expandedAccountInfo;
                }

                return (pulledAccount);
            }
            catch (TaskCanceledException pTaskCancelEx)
            {
                //log exception
                throw new InvalidOperationException(
                    $"Unable to locate account ({_uniqueId} - MTG)",
                    pTaskCancelEx);
            }
        }

        private void ClearSessionRetrievedAccount()
        {
            _session.RetrievedAccount = null;
        }

        #endregion
    }
}
