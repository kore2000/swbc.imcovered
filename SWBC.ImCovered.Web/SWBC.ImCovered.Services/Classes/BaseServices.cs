﻿using System;

namespace SWBC.ImCovered.Services.Classes
{
    public abstract class BaseServices
    {
        #region <Private members>

        #endregion

        #region <Public accessors>

        #endregion

        #region <Constructors>

        protected BaseServices()
        {
        }

        #endregion

        protected void ThrowExceptionIfEmpty(
            string pValue,
            string pParameterName)
        {
            if (string.IsNullOrWhiteSpace(pValue))
            {
                throw new ArgumentNullException(pParameterName);
            }
        }
    }
}
