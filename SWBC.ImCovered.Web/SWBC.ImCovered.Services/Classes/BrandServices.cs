﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using System.Threading;

using SWBC.ImCovered.Services.Interfaces;
using SWBC.ImCovered.DAL.Classes.DataTransferObjects;
using SWBC.ImCovered.DAL;


namespace SWBC.ImCovered.Services.Classes
{
    public class BrandServices:
        BaseServices,
        IBrandServices
    {
        #region <Private members>

        private readonly ImCoveredContext _dbContext;

        #endregion

        #region <Public accessors>

        #endregion

        #region <Constructors>

        public BrandServices(
            ImCoveredContext pDbContext):
            base()
        {
            _dbContext = pDbContext ??
                throw new ArgumentNullException(
                    nameof(pDbContext));
        }

        #endregion

        public async Task<Brand> GetBrandAsync(
            string pName)
        {
            ThrowExceptionIfEmpty(
                pName,
                nameof(pName));

            using (var cancelTokenSource = new CancellationTokenSource(15000))
            {
                try
                {
                    var retrievedBrand = await _dbContext
                        .Brands
                        .Include(i => i.UrlShortName)
                        .FirstOrDefaultAsync(
                            fod => fod.IsActive &&
                            fod.UrlShortName.Name.Equals(
                                pName,
                                StringComparison.CurrentCultureIgnoreCase),
                            cancelTokenSource.Token);

                    if (retrievedBrand == null)
                    {
                        throw new ArgumentException(
                            "Unable to locate request brand",
                            nameof(pName));
                    }

                    return (retrievedBrand);
                }
                catch (TaskCanceledException pTaskCancelEx)
                {
                    //log exception
                    throw new InvalidOperationException(
                        $"Unable to locate brand ({pName})",
                        pTaskCancelEx);
                }
            }
        }
    }
}
