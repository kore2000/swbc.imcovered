﻿using Microsoft.EntityFrameworkCore;
using System;
using Microsoft.Extensions.Configuration;

using SWBC.ImCovered.Services.Interfaces;
using SWBC.ImCovered.DAL;

namespace SWBC.ImCovered.Services.Classes
{
    public class SiteServices :
        ISiteServices
    {
        #region <Private members>

        private readonly DbContext _dbContext;
        private readonly IConfiguration _config;

        #endregion

        #region <Public accessors>

        #endregion

        #region <Constructors>

        public SiteServices(
            IConfiguration pConfig,
            DbContext pDbContext)
        {
            _config = pConfig ??
                throw new ArgumentNullException(
                    nameof(pConfig));
            _dbContext = pDbContext ??
                throw new ArgumentNullException(
                    nameof(pDbContext));
        }

        public SiteServices(
            IConfiguration pConfig,
            ImCoveredContext pDbContext):
            this(pConfig, pDbContext as DbContext)
        {
        }

        #endregion

        public int GetYear()
        {
            return (DateTime.Now.Year);
        }

        public string GetReleaseBuild()
        {
            var releaseBuild = _config
                .GetValue<string>("appSettings:ReleaseBuild");
            if (string.IsNullOrWhiteSpace(releaseBuild))
            {
                return (string.Empty);
            }

            return (releaseBuild);
        }

        public string GetReleaseTag()
        {
            var releaseTag = _config
                .GetValue<string>("appSettings:ReleaseTag");
            if (string.IsNullOrWhiteSpace(releaseTag))
            {
                return (string.Empty);
            }

            return (releaseTag);
        }

        public bool GetDisplayBuildInfo()
        {
            return (_config
                .GetValue<bool>("appSettings:DisplayReleaseBuild"));
        }
    }
}
