﻿using System.Threading.Tasks;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;
using SWBC.ImCovered.Enums.Classes;

namespace SWBC.ImCovered.Services.Interfaces
{
    public interface IPolicyServices
    {
        Task<PolicyLookupEnums.Status> PolicyLookupByUid(
            string pUid,
            int pZip);

        Task<CpiAccountInformationFI100> GetPolicyInformation(
            string pUid);

        Task<PolicyLookupEnums.Status> PolicyLookupByName(
            string pName,
            int pZip,
            DbContextEnums.InsuranceType pInsuranceType);

        double GetSecureIdFromSession();
    }
}
