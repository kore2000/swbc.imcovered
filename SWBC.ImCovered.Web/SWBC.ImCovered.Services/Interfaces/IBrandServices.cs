﻿using System.Threading.Tasks;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;

namespace SWBC.ImCovered.Services.Interfaces
{
    public interface IBrandServices
    {
        Task<Brand> GetBrandAsync(
            string pName);
    }
}
