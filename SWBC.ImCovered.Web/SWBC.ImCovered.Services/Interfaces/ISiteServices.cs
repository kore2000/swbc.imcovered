﻿namespace SWBC.ImCovered.Services.Interfaces
{
    public interface ISiteServices
    {
        string GetReleaseBuild();
        string GetReleaseTag();
        int GetYear();
        bool GetDisplayBuildInfo();
    }
}