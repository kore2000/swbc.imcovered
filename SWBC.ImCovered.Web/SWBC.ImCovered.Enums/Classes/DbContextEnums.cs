﻿namespace SWBC.ImCovered.Enums.Classes
{
    public class DbContextEnums
    {
        public enum AgencyNumber
        {
            None = 0,
            AgencyOne = 1,
            AgencyTwentyOne = 21,
            AgencyTwentyTwo = 22,
            AgencyNinetyNine = 99
        }

        public enum InsuranceType
        {
            None = 0,
            Cpi,
            Mortgage
        }
    }
}
