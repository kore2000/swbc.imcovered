﻿namespace SWBC.ImCovered.Enums.Classes
{
    public class PolicyLookupEnums
    {
        public enum Status
        {
            None = 0,
            UidFoundAuthenticated,
            UidFoundNotAuthenticated,
            UidNotFound,
            NoAccountsFound,
            SingleAccountFound,
            MultipleAccountsFound
        }
    }
}
