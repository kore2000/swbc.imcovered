﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SWBC.ImCovered.Models
{
    public class FooterBuildInfoComponentModel
    {
        #region <Private members>

        private int _year;
        private string _releaseTag;
        private string _releaseBuild;
        private bool _displayBuildInfo;

        #endregion

        #region <Public accessors>

        public int Year
        {
            get => (_year);
            set => _year = value;
        }

        public string ReleaseTag
        {
            get => (_releaseTag);
            set => _releaseTag = value;
        }

        public string ReleaseBuild
        {
            get => (_releaseBuild);
            set => _releaseBuild = value;
        }

        public bool DisplayBuildInfo
        {
            get => (_displayBuildInfo);
            set => _displayBuildInfo = value;
        }

        #endregion
    }
}
