﻿using Newtonsoft.Json;
using Microsoft.AspNetCore.Html;
using System.ComponentModel.DataAnnotations;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;
using SWBC.ImCovered.Resources;
using SWBC.ImCovered.Enums.Classes;

namespace SWBC.ImCovered.Models
{
    public class MultipleAccountDocumentUploadModel
    {
        #region <Private members>

        private string _lastName;
        private int _zipCode;
        private DbContextEnums.InsuranceType _loanType;

        #endregion

        #region <Public accessors>

        [Required(AllowEmptyStrings =false)]
        [DataType(DataType.Text)]
        [RegularExpression("^[A-Za-z]{1,30}$")]
        [Display(Name = "LastName", ResourceType = typeof(CommonTerms))]
        public string LastName
        {
            get => (_lastName);
            set => _lastName = value;
        }

        [Required(AllowEmptyStrings = false)]
        [RegularExpression("^[0-9]{5}$")]
        [DataType(DataType.PostalCode)]
        [Display(Name = "ZipCode", ResourceType = typeof(CommonTerms))]
        public int ZipCode
        {
            get => (_zipCode);
            set => _zipCode = value;
        }

        [Required(AllowEmptyStrings = false)]
        public DbContextEnums.InsuranceType LoanType
        {
            get => (_loanType);
            set => _loanType = value;
        }

        #endregion
    }
}
