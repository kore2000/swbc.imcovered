﻿using Newtonsoft.Json;
using Microsoft.AspNetCore.Html;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;

namespace SWBC.ImCovered.Models
{
    public class LoginModel
    {
        #region <Private members>

        private int? _uid;
        private string _activeTab;
        private string _baseUrl;
        private Brand _brand;

        #endregion

        #region <Public accessors>

        public int? Uid
        {
            get => (_uid);
            set => _uid = value;
        }

        public string ActiveTab
        {
            get => (_activeTab);
            set => _activeTab = value;
        }

        public string BaseUrl
        {
            get => (_baseUrl);
            set => _baseUrl = value;
        }

        public Brand Brand
        {
            get => (_brand);
            set => _brand = value;
        }

        #endregion

        public IHtmlContent GetJson()
        {
            var objToReturn = new
            {
                uid = _uid.HasValue ? _uid.Value.ToString() : "",
                activeTab = _activeTab,
                baseUrl = _baseUrl
            };

            return (new HtmlString(JsonConvert
                .SerializeObject(
                    objToReturn)));
        }
    }
}
