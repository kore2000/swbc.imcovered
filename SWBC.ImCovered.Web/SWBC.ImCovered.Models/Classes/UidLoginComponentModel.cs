﻿using Newtonsoft.Json;
using Microsoft.AspNetCore.Html;
using System.ComponentModel.DataAnnotations;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;
using SWBC.ImCovered.Resources;

namespace SWBC.ImCovered.Models
{
    public class UidLoginComponentModel
    {
        #region <Private members>

        private string _uid;
        private int _zipCode;

        #endregion

        #region <Public accessors>

        [Required(AllowEmptyStrings =false)]
        [DataType(DataType.Text)]
        [RegularExpression("^[0-9]{5,12}$")]
        [Display(Name = "UniqueIdentifier", ResourceType = typeof(CommonTerms))]
        public string Uid
        {
            get => (_uid);
            set => _uid = value;
        }

        [Required(AllowEmptyStrings = false)]
        [RegularExpression("^[0-9]{5}$")]
        [DataType(DataType.PostalCode)]
        [Display(Name = "ZipCode", ResourceType = typeof(CommonTerms))]
        public int ZipCode
        {
            get => (_zipCode);
            set => _zipCode = value;
        }

        #endregion
    }
}
