﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.AspNetCore.Http;

using SWBC.ImCovered.DAL.Classes.DataTransferObjects;
using Custom = SWBC.ImCovered.StateManagement.Interfaces;

namespace SWBC.ImCovered.StateManagement.Classes
{
    public class Session :
        Custom.ISession
    {
        #region <Private members>

        private readonly ISession _session;

        #endregion

        #region <Public accessors>

        public Brand SiteBrand
        {
            get => (GetBrand());
            set => SetBrand(value);
        }

        public BaseAccountInformation RetrievedAccount
        {
            get => (GetRetrievedAccount());
            set => SetRetrievedAccount(value);
        }

        #endregion

        public Session(
            IHttpContextAccessor pHttpContext)
        {
            var httpContext = pHttpContext ??
                throw new ArgumentNullException(
                    nameof(pHttpContext));

            _session = httpContext.HttpContext.Session;
        }

        #region <Private methods>

        private void ThrowExceptionIfSessionNull()
        {
            if (_session == null)
            {
                throw new InvalidOperationException(
                    "Session object not set");
            }
        }

        private void ClearValueIfNull(
            object pObject,
            string pSessionName)
        {
            if (pObject == null)
            {
                _session.Remove(pSessionName);
            }
        }

        private T ConvertFromByteArray<T>(
            byte[] pObjectInBytes)
        {
            if (pObjectInBytes == null)
            {
                return (default(T));
            }

            var binaryFormatter = new BinaryFormatter();
            using (var memoryStream = new MemoryStream(
                pObjectInBytes))
            {
                var returnObj = binaryFormatter
                    .Deserialize(memoryStream);
                return ((T)returnObj);
            }
        }

        private byte[] ConvertToByteArray<T>(
            T pObject)
        {
            if (pObject == null)
            {
                return (null);
            }

            var binaryFormatter = new BinaryFormatter();
            using (var memoryStream = new MemoryStream())
            {
                binaryFormatter
                    .Serialize(memoryStream, pObject);
                return (memoryStream.ToArray());
            }
        }

        private Brand GetBrand()
        {
            ThrowExceptionIfSessionNull();

            if (_session.TryGetValue(
                    Names.SiteBrand,
                    out byte[] brandBytes))
            {
                try
                {
                    return (ConvertFromByteArray<Brand>(brandBytes));
                }
                catch (Exception pEx)
                {
                    //log exception
                    return (null);
                }
            }

            return (null);
        }

        private BaseAccountInformation GetRetrievedAccount()
        {
            ThrowExceptionIfSessionNull();

            if (_session.TryGetValue(
                    Names.RetrievedAccount,
                    out byte[] brandBytes))
            {
                try
                {
                    return (ConvertFromByteArray<BaseAccountInformation>(brandBytes));
                }
                catch (Exception pEx)
                {
                    //log exception
                    return (null);
                }
            }

            return (null);
        }

        private void SetBrand(
            Brand pBrand)
        {
            ThrowExceptionIfSessionNull();
            ClearValueIfNull(
                pBrand,
                Names.SiteBrand);

            if (pBrand == null)
            {
                return;
            }

            var serializedObj = ConvertToByteArray(
                pBrand);
            if (serializedObj == null)
            {
                throw new InvalidOperationException(
                    "Could not convert byte array");
            }

            try
            {
                _session.Set(
                    Names.SiteBrand,
                    serializedObj);
            }
            catch (Exception pEx)
            {
                //log exception
            }
        }

        private void SetRetrievedAccount(
            BaseAccountInformation pAccount)
        {
            ThrowExceptionIfSessionNull();
            ClearValueIfNull(
                pAccount,
                Names.RetrievedAccount);

            if (pAccount == null)
            {
                return;
            }

            var serializedObj = ConvertToByteArray(
                pAccount);
            if (serializedObj == null)
            {
                throw new InvalidOperationException(
                    "Could not convert byte array");
            }

            try
            {
                _session.Set(
                    Names.RetrievedAccount,
                    serializedObj);
            }
            catch (Exception pEx)
            {
                //log exception
            }
        }

        #endregion

        private struct Names
        {
            public readonly static string SiteBrand;
            public readonly static string RetrievedAccount;

            static Names()
            {
                SiteBrand = "SiteBrand";
                RetrievedAccount = "RetrievedAccount";
            }
        }
    }
}
