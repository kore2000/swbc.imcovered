﻿using SWBC.ImCovered.DAL.Classes.DataTransferObjects;

namespace SWBC.ImCovered.StateManagement.Interfaces
{
    public interface ISession
    {
        Brand SiteBrand { get; set; }
        BaseAccountInformation RetrievedAccount { get; set; }
    }
}